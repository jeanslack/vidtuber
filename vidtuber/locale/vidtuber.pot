# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-16 13:06+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../gui_app.py:164
msgid ""
"Permission denied: {}\n"
"\n"
"Check execution permissions."
msgstr ""

#: ../gui_app.py:244
#, python-brace-format
msgid ""
"Unexpected error while deleting file contents:\n"
"\n"
"{0}"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:59
msgid "Please take a moment to set up the application"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:60
msgid "Click the \"Next\" button to get started"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:75
msgid "Welcome to the Vidtuber Wizard!"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:117
msgid "Vidtuber is an application based on FFmpeg\n"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:119
msgid "If FFmpeg is not on your computer, this application will be unusable"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:122
msgid ""
"If you have already installed FFmpeg on your operating\n"
"system, click the \"Auto-detection\" button."
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:125
msgid ""
"If you want to use a version of FFmpeg located on your\n"
"filesystem but not installed on your operating system,\n"
"click the \"Locate\" button."
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:147
msgid "Auto-detection"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:149
msgid "Locate"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:200
msgid "Click the \"Next\" button"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:221
msgid ""
"'{}' is not installed on your computer. Install it or indicate another "
"location by clicking the 'Locate' button."
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:235
msgid ""
"Vidtuber already seems to include FFmpeg.\n"
"\n"
"Do you want to use that?"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:237
msgid "Vidtuber: Please Confirm"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:261
msgid "Locating FFmpeg executables\n"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:263
msgid ""
"\"ffmpeg\", \"ffprobe\" and \"ffplay\" are required. Complete all\n"
"the text boxes below by clicking on the respective buttons."
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:345 ../vdms_dialogs/wizard_dlg.py:362
#: ../vdms_dialogs/wizard_dlg.py:380 ../vdms_dialogs/preferences.py:793
#: ../vdms_dialogs/preferences.py:835 ../vdms_dialogs/preferences.py:878
msgid "Choose the {} executable"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:403
msgid ""
"Vidtuber has a simple graphical\n"
"interface for youtube-dl and yt-dlp\n"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:406
msgid ""
"This feature allows you to download video and audio from\n"
"many sites, including YouTube.com and even Facebook."
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:425
msgid " Do you want to enable this feature?"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:428 ../vdms_dialogs/preferences.py:287
msgid "Downloader preferences"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:484
msgid "Wizard completed successfully!\n"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:485
msgid ""
"Remember that you can always change these settings later, through the Setup "
"dialog."
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:487
msgid "Thank You!"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:488
msgid "To exit click the \"Finish\" button"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:580
msgid "< Previous"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:583 ../vdms_dialogs/wizard_dlg.py:683
msgid "Next >"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:588
msgid "Vidtuber Wizard"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:617 ../vdms_dialogs/wizard_dlg.py:653
msgid "Finish"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:645
msgid "Some text boxes are still incomplete"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:715
msgid "Re-start is required"
msgstr ""

#: ../vdms_dialogs/wizard_dlg.py:716
msgid "Done!"
msgstr ""

#: ../vdms_dialogs/infoprg.py:62
msgid "Cross-platform graphical interface for FFmpeg and youtube-dl.\n"
msgstr ""

#: ../vdms_dialogs/playlist_indexing.py:50 ../vdms_dialogs/ydl_mediainfo.py:68
msgid "URL"
msgstr ""

#: ../vdms_dialogs/playlist_indexing.py:51
msgid "Playlist Items"
msgstr ""

#: ../vdms_dialogs/playlist_indexing.py:65
msgid ""
"Click on \"Playlist Items\" column to specify indices of the videos in the "
"playlist separated by commas like: \"1,2,5,8\" if you want to download "
"videos indexed 1, 2, 5, 8 in the playlist.\n"
"\n"
"You can specify range: \"1-3,7,10-13\" it will download the videos at index "
"1, 2, 3, 7, 10, 11, 12 and 13.\n"
msgstr ""

#: ../vdms_dialogs/playlist_indexing.py:101
msgid "Playlist video items to download"
msgstr ""

#: ../vdms_dialogs/playlist_indexing.py:110 ../vdms_panels/youtubedl_ui.py:365
msgid "Help viewer"
msgstr ""

#: ../vdms_dialogs/playlist_indexing.py:121
msgid "Reset"
msgstr ""

#: ../vdms_dialogs/playlist_indexing.py:125
msgid "Apply"
msgstr ""

#: ../vdms_dialogs/playlist_indexing.py:215
msgid "ERROR: Invalid option"
msgstr ""

#: ../vdms_dialogs/playlist_indexing.py:216
msgid "please try again."
msgstr ""

#: ../vdms_dialogs/playlist_indexing.py:217
msgid "OK: Indexes to download"
msgstr ""

#: ../vdms_dialogs/playlist_indexing.py:246
msgid ""
"WARNING: The selected URL does not refer to a playlist. Only lines marked "
"green can be indexed."
msgstr ""

#: ../vdms_dialogs/preferences.py:88
msgid "Application Language"
msgstr ""

#: ../vdms_dialogs/preferences.py:99
msgid "Clear the cache when exiting the application"
msgstr ""

#: ../vdms_dialogs/preferences.py:103
msgid "Delete the contents of the log files when exiting the application"
msgstr ""

#: ../vdms_dialogs/preferences.py:109
msgid "Warn on exit"
msgstr ""

#: ../vdms_dialogs/preferences.py:114
msgid "Miscellanea"
msgstr ""

#: ../vdms_dialogs/preferences.py:120
msgid "Where do you prefer to save your transcodes?"
msgstr ""

#: ../vdms_dialogs/preferences.py:136
msgid "Save each file in the same folder as input file"
msgstr ""

#: ../vdms_dialogs/preferences.py:141
msgid "Assign optional suffix to output file names:"
msgstr ""

#: ../vdms_dialogs/preferences.py:148
msgid "Move source file to the Vidtuber trash folder after successful encoding"
msgstr ""

#: ../vdms_dialogs/preferences.py:165
msgid "Where do you prefer to save your downloads?"
msgstr ""

#: ../vdms_dialogs/preferences.py:180
msgid "Auto-create subfolders when downloading playlists"
msgstr ""

#: ../vdms_dialogs/preferences.py:185
msgid "File Preferences"
msgstr ""

#: ../vdms_dialogs/preferences.py:192
msgid "Path to the executables"
msgstr ""

#: ../vdms_dialogs/preferences.py:195
msgid "Enable another location to run FFmpeg"
msgstr ""

#: ../vdms_dialogs/preferences.py:207
msgid "Enable another location to run FFprobe"
msgstr ""

#: ../vdms_dialogs/preferences.py:219
msgid "Enable another location to run FFplay"
msgstr ""

#: ../vdms_dialogs/preferences.py:231
msgid "Other options"
msgstr ""

#: ../vdms_dialogs/preferences.py:235
msgid "Threads used for transcoding (from 0 to 32):"
msgstr ""

#: ../vdms_dialogs/preferences.py:253
msgid "FFmpeg"
msgstr ""

#: ../vdms_dialogs/preferences.py:265
msgid "How to upgrade a Python package"
msgstr ""

#: ../vdms_dialogs/preferences.py:272
msgid ""
"Required: Microsoft Visual C++ 2010 Service Pack 1 Redistributable Package "
"(x86)"
msgstr ""

#: ../vdms_dialogs/preferences.py:281
msgid "How to install pip on your Linux distribution"
msgstr ""

#: ../vdms_dialogs/preferences.py:298
msgid "Current path:"
msgstr ""

#: ../vdms_dialogs/preferences.py:306
msgid ""
"Make sure you are using the latest available version of\n"
"'{}'. This allows you to avoid download problems.\n"
msgstr ""

#: ../vdms_dialogs/preferences.py:314 ../vdms_dialogs/preferences.py:330
#: ../vdms_dialogs/preferences.py:346
msgid "Disabled"
msgstr ""

#: ../vdms_dialogs/preferences.py:325
msgid "Not found"
msgstr ""

#: ../vdms_dialogs/preferences.py:342 ../vdms_dialogs/preferences.py:358
msgid "Not Installed"
msgstr ""

#: ../vdms_dialogs/preferences.py:363
msgid "Downloader"
msgstr ""

#: ../vdms_dialogs/preferences.py:369
msgid "Icon themes"
msgstr ""

#: ../vdms_dialogs/preferences.py:371
msgid ""
"The chosen icon theme will only change the icons,\n"
"background and foreground of some text fields."
msgstr ""

#: ../vdms_dialogs/preferences.py:389
msgid "Toolbar customization"
msgstr ""

#: ../vdms_dialogs/preferences.py:391
msgid "At the top of window (default)"
msgstr ""

#: ../vdms_dialogs/preferences.py:392
msgid "At the bottom of window"
msgstr ""

#: ../vdms_dialogs/preferences.py:393
msgid "At the right of window"
msgstr ""

#: ../vdms_dialogs/preferences.py:394
msgid "At the left of window"
msgstr ""

#: ../vdms_dialogs/preferences.py:396
msgid "Place the toolbar"
msgstr ""

#: ../vdms_dialogs/preferences.py:406
msgid "Icon size:"
msgstr ""

#: ../vdms_dialogs/preferences.py:419
msgid "Shows the text in the toolbar buttons"
msgstr ""

#: ../vdms_dialogs/preferences.py:424
msgid "Appearance"
msgstr ""

#: ../vdms_dialogs/preferences.py:431
msgid ""
"The following settings affect output messages and\n"
"the log messages during transcoding processes.\n"
"Change only if you know what you are doing.\n"
msgstr ""

#: ../vdms_dialogs/preferences.py:452
msgid "FFmpeg logging levels"
msgstr ""

#: ../vdms_dialogs/preferences.py:472 ../vdms_main/main_frame.py:315
msgid "Settings"
msgstr ""

#: ../vdms_dialogs/preferences.py:496
msgid "By assigning an additional suffix you could avoid overwriting files"
msgstr ""

#: ../vdms_dialogs/preferences.py:627
msgid "Set a persistent location to save the file downloads"
msgstr ""

#: ../vdms_dialogs/preferences.py:650
msgid "Set a persistent location to save exported files"
msgstr ""

#: ../vdms_dialogs/preferences.py:682
msgid ""
"Enter only alphanumeric characters. You can also use the hyphen (\"-\") and "
"the underscore (\"_\"). Spaces are not allowed."
msgstr ""

#: ../vdms_dialogs/preferences.py:735
msgid "Choose a new destination for the files to be trashed"
msgstr ""

#: ../vdms_dialogs/preferences.py:985
msgid ""
"Changes will take effect once the program has been restarted.\n"
"\n"
"Do you want to exit the application now?"
msgstr ""

#: ../vdms_dialogs/preferences.py:988 ../vdms_main/main_frame.py:213
msgid "Exit"
msgstr ""

#: ../vdms_dialogs/vidtuber_check_version.py:63
msgid "Get Latest Version"
msgstr ""

#: ../vdms_dialogs/vidtuber_check_version.py:67
msgid "Checking for newer version"
msgstr ""

#: ../vdms_dialogs/vidtuber_check_version.py:95
msgid ""
"\n"
"\n"
"New releases fix bugs and offer new features."
msgstr ""

#: ../vdms_dialogs/vidtuber_check_version.py:96
#, python-brace-format
msgid ""
"\n"
"\n"
"This is Vidtuber v.{0}\n"
"\n"
msgstr ""

#: ../vdms_dialogs/ydl_mediainfo.py:64
msgid "Statistics viewer"
msgstr ""

#: ../vdms_dialogs/ydl_mediainfo.py:67
msgid "TITLE SELECTION"
msgstr ""

#: ../vdms_dialogs/showlogs.py:74
msgid "Log file list"
msgstr ""

#: ../vdms_dialogs/showlogs.py:76
msgid "Log messages"
msgstr ""

#: ../vdms_dialogs/showlogs.py:99
msgid "Refresh all log files"
msgstr ""

#: ../vdms_dialogs/showlogs.py:104
msgid "Clear selected log"
msgstr ""

#: ../vdms_dialogs/showlogs.py:114
msgid "Showing log messages"
msgstr ""

#: ../vdms_dialogs/showlogs.py:141
msgid "Select a log file"
msgstr ""

#: ../vdms_dialogs/showlogs.py:148
msgid "Are you sure you want to clear the selected log file?"
msgstr ""

#: ../vdms_io/io_tools.py:62
msgid "Vidtuber - Loading..."
msgstr ""

#: ../vdms_io/io_tools.py:63
msgid ""
"Wait....\n"
"Retrieving required data."
msgstr ""

#: ../vdms_main/main_frame.py:115 ../vdms_main/main_frame.py:161
#: ../vdms_main/main_frame.py:789 ../vdms_main/main_frame.py:829
#: ../vdms_panels/youtubedl_ui.py:521 ../vdms_panels/youtubedl_ui.py:756
msgid "Ready"
msgstr ""

#: ../vdms_main/main_frame.py:160
msgid "Vidtuber"
msgstr ""

#: ../vdms_main/main_frame.py:212
msgid "Are you sure you want to exit?"
msgstr ""

#: ../vdms_main/main_frame.py:244
msgid "Downloads folder\tCtrl+D"
msgstr ""

#: ../vdms_main/main_frame.py:245
msgid "Open the default downloads folder"
msgstr ""

#: ../vdms_main/main_frame.py:248
msgid "Open temporary downloads"
msgstr ""

#: ../vdms_main/main_frame.py:249
msgid "Open the temporary downloads folder"
msgstr ""

#: ../vdms_main/main_frame.py:254
msgid "Work Notes\tCtrl+N"
msgstr ""

#: ../vdms_main/main_frame.py:255
msgid "Read and write useful notes and reminders."
msgstr ""

#: ../vdms_main/main_frame.py:258
msgid "Exit\tCtrl+Q"
msgstr ""

#: ../vdms_main/main_frame.py:259
msgid "Close Vidtuber"
msgstr ""

#: ../vdms_main/main_frame.py:260
msgid "File"
msgstr ""

#: ../vdms_main/main_frame.py:264
msgid "Version in Use"
msgstr ""

#: ../vdms_main/main_frame.py:265
msgid "Shows the version in use"
msgstr ""

#: ../vdms_main/main_frame.py:267
msgid "Show the latest version..."
msgstr ""

#: ../vdms_main/main_frame.py:268
msgid "Shows the latest version available on github.com"
msgstr ""

#: ../vdms_main/main_frame.py:272
msgid "Show Logs\tCtrl+L"
msgstr ""

#: ../vdms_main/main_frame.py:273
msgid "Viewing log messages"
msgstr ""

#: ../vdms_main/main_frame.py:275
msgid "View"
msgstr ""

#: ../vdms_main/main_frame.py:283
msgid "Output Monitor\tShift+O"
msgstr ""

#: ../vdms_main/main_frame.py:284
msgid "Keeps track of the output for debugging errors"
msgstr ""

#: ../vdms_main/main_frame.py:288
msgid "Configuration folder"
msgstr ""

#: ../vdms_main/main_frame.py:289
msgid "Opens the Vidtuber configuration folder"
msgstr ""

#: ../vdms_main/main_frame.py:291
msgid "Log folder"
msgstr ""

#: ../vdms_main/main_frame.py:292
msgid "Opens the Vidtuber log folder, if exists"
msgstr ""

#: ../vdms_main/main_frame.py:294
msgid "System"
msgstr ""

#: ../vdms_main/main_frame.py:296
msgid "Goto"
msgstr ""

#: ../vdms_main/main_frame.py:301 ../vdms_panels/textdrop.py:83
msgid "Set up a temporary folder for downloads"
msgstr ""

#: ../vdms_main/main_frame.py:302
msgid "Save all downloads to this temporary location"
msgstr ""

#: ../vdms_main/main_frame.py:305
msgid "Restore the default destination folders"
msgstr ""

#: ../vdms_main/main_frame.py:306
msgid "Restore the default folders for file conversions and downloads"
msgstr ""

#: ../vdms_main/main_frame.py:313
msgid "Preferences\tCtrl+P"
msgstr ""

#: ../vdms_main/main_frame.py:314
msgid "Application preferences"
msgstr ""

#: ../vdms_main/main_frame.py:319
msgid "Wiki"
msgstr ""

#: ../vdms_main/main_frame.py:321
msgid "Issue tracker"
msgstr ""

#: ../vdms_main/main_frame.py:323
msgid "Translation..."
msgstr ""

#: ../vdms_main/main_frame.py:325
msgid "Donation"
msgstr ""

#: ../vdms_main/main_frame.py:328
msgid "Check for newer version"
msgstr ""

#: ../vdms_main/main_frame.py:329
msgid ""
"Check for the latest Vidtuber version at <https://pypi.org/project/vidtuber/>"
msgstr ""

#: ../vdms_main/main_frame.py:333
msgid "System version"
msgstr ""

#: ../vdms_main/main_frame.py:334
msgid ""
"Get version about your operating system, version of Python and wxPython."
msgstr ""

#: ../vdms_main/main_frame.py:337
msgid "About Vidtuber"
msgstr ""

#: ../vdms_main/main_frame.py:338
msgid "Help"
msgstr ""

#: ../vdms_main/main_frame.py:412
#, python-brace-format
msgid ""
"Unexpected error while creating file:\n"
"\n"
"{0}"
msgstr ""

#: ../vdms_main/main_frame.py:430
#, python-brace-format
msgid "You are using '{0}' version {1}"
msgstr ""

#: ../vdms_main/main_frame.py:455
#, python-brace-format
msgid "{0}: Latest version available: {1}"
msgstr ""

#: ../vdms_main/main_frame.py:498
msgid "Choose a temporary destination for downloads"
msgstr ""

#: ../vdms_main/main_frame.py:528
msgid "Default destination folders successfully restored"
msgstr ""

#: ../vdms_main/main_frame.py:610
#, python-brace-format
msgid "A new release is available - v.{0}\n"
msgstr ""

#: ../vdms_main/main_frame.py:613
msgid "You are using a development version that has not yet been released!\n"
msgstr ""

#: ../vdms_main/main_frame.py:616
msgid "Congratulation! You are already using the latest version.\n"
msgstr ""

#: ../vdms_main/main_frame.py:698
msgid "Go to the previous panel"
msgstr ""

#: ../vdms_main/main_frame.py:699
msgid "Back"
msgstr ""

#: ../vdms_main/main_frame.py:703
msgid "Go to the next panel"
msgstr ""

#: ../vdms_main/main_frame.py:704
msgid "Next"
msgstr ""

#: ../vdms_main/main_frame.py:708
msgid "Shows download statistics and information"
msgstr ""

#: ../vdms_main/main_frame.py:709
msgid "Statistics"
msgstr ""

#: ../vdms_main/main_frame.py:715
msgid "Start downloading"
msgstr ""

#: ../vdms_main/main_frame.py:716
msgid "Download"
msgstr ""

#: ../vdms_main/main_frame.py:755
msgid "ERROR: Invalid URL: \"{}\""
msgstr ""

#: ../vdms_main/main_frame.py:759
msgid "ERROR: Some equal URLs found"
msgstr ""

#: ../vdms_main/main_frame.py:790
msgid "Vidtuber - Queued URLs"
msgstr ""

#: ../vdms_main/main_frame.py:816
msgid "URL list changed, please check the settings again."
msgstr ""

#: ../vdms_main/main_frame.py:831
msgid "Vidtuber - YouTube Downloader"
msgstr ""

#: ../vdms_main/main_frame.py:859
msgid "Viewing last log"
msgstr ""

#: ../vdms_main/main_frame.py:865
msgid "Vidtuber - Output Monitor"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:133
msgid ""
"At least one \"Format Code\" must be checked for each URL selected in green."
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:167
msgid "Precompiled Videos"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:168
msgid "Download videos by resolution"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:169
msgid "Download split audio and video"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:170
msgid "Download Audio only"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:171
msgid "Download by format code"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:211
msgid "Options"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:245
msgid ""
"When not available, the chosen video resolution will be replaced with the "
"closest one"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:249
msgid "Preferred video format"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:282
msgid "Preview"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:288
msgid "Download all videos in playlist"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:293
msgid "Playlist Editor"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:299
msgid "Don’t check SSL certificate"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:303
msgid "Embed thumbnail in audio file"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:307
msgid "Add metadata to file"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:311
msgid "Write subtitles to video"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:316
msgid "Download all available subtitles"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:324
msgid "Download subtitles only"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:331
msgid "Prevent overwriting files"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:335
msgid ""
"Include the video ID\n"
"in the file names"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:341
msgid "Restrict file names"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:389 ../vdms_panels/youtubedl_ui.py:488
#: ../vdms_panels/youtubedl_ui.py:504
msgid "Play selected url"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:516
msgid "An item must be selected in the URLs checklist"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:532
#, python-brace-format
msgid ""
"Only one video can be played at a time.\n"
"\n"
"Unsupported '{0}':\n"
"'{1}'"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:567
msgid "Format Code"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:568 ../vdms_panels/youtubedl_ui.py:677
msgid "Url"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:569 ../vdms_panels/youtubedl_ui.py:678
msgid "Title"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:570
msgid "Extension"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:571
msgid "Resolution"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:572
msgid "Video Codec"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:573
msgid "fps"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:574
msgid "Audio Codec"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:575
msgid "Size"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:613
#, python-brace-format
msgid ""
"ERROR: Unable to get format codes.\n"
"\n"
"Unsupported URL:\n"
"'{0}'"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:679
msgid "Quality"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:736
#, python-brace-format
msgid ""
"Unable to get format codes on '{0}'\n"
"\n"
"Unsupported '{0}':\n"
"'{1}'"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:893
msgid "URLs have no playlist references"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:1026
msgid "The URLs contain playlists. Are you sure you want to continue?"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:1028 ../vdms_panels/youtubedl_ui.py:1034
#: ../vdms_panels/long_processing_task.py:282
msgid "Please confirm"
msgstr ""

#: ../vdms_panels/youtubedl_ui.py:1032
msgid "The URLs contain channels. Are you sure you want to continue?"
msgstr ""

#: ../vdms_panels/long_processing_task.py:47
msgid "[Vidtuber]: SUCCESS !"
msgstr ""

#: ../vdms_panels/long_processing_task.py:48
msgid "[Vidtuber]: FAILED !"
msgstr ""

#: ../vdms_panels/long_processing_task.py:49
msgid "Sorry, all task failed !"
msgstr ""

#: ../vdms_panels/long_processing_task.py:50
msgid "The process was stopped due to a fatal error."
msgstr ""

#: ../vdms_panels/long_processing_task.py:51
msgid "Interrupted Process !"
msgstr ""

#: ../vdms_panels/long_processing_task.py:52
msgid "Successfully completed !"
msgstr ""

#: ../vdms_panels/long_processing_task.py:53
msgid "Not everything was successful."
msgstr ""

#: ../vdms_panels/long_processing_task.py:80
msgid "Process log:"
msgstr ""

#: ../vdms_panels/long_processing_task.py:90
msgid "Abort"
msgstr ""

#: ../vdms_panels/long_processing_task.py:103
msgid "Stops current process"
msgstr ""

#: ../vdms_panels/long_processing_task.py:217
msgid "Fatal Error !"
msgstr ""

#: ../vdms_panels/long_processing_task.py:226
msgid "Get your files at the destination you specified"
msgstr ""

#: ../vdms_panels/long_processing_task.py:234
#: ../vdms_panels/long_processing_task.py:241
msgid ""
"Check the current output or read the related log file for more information."
msgstr ""

#: ../vdms_panels/long_processing_task.py:246
msgid "...Finished"
msgstr ""

#: ../vdms_panels/long_processing_task.py:260
msgid ""
"wait... all operations will be stopped at the end of the download in "
"progress "
msgstr ""

#: ../vdms_panels/long_processing_task.py:279
msgid ""
"There are still processes running.. if you want to stop them, use the \"Abort"
"\" button.\n"
"\n"
"Do you want to kill application?"
msgstr ""

#: ../vdms_panels/textdrop.py:47
msgid "Enter URLs below"
msgstr ""

#: ../vdms_panels/textdrop.py:82
msgid "Delete all text from the list"
msgstr ""

#: ../vdms_panels/textdrop.py:85
msgid "Destination folder"
msgstr ""
