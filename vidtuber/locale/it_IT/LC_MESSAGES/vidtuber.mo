��    �      �  %  l         /     $   1  w   V  p   �     ?  
   K  #   V     z     �  .   �  
   �     �     �     �  5        8  ,   W  J   �     �     �     �          3  1   ?     q     �  C   �  g   �     1  K   I     �  4   �  ,   �          +  ,   >  #  k     �  &   �     �  :   �  >        ]  1   k     �  A   �     �               "     (     F     O      c     �     �     �     �     �  
             3     K     d     z  :   �     �  %   �  %     &   <     c  w   t     �     �  	   �               $     2     7     H     O     [  H   n  /   �     �     �                 -   +     Y  
   y     �  D   �  a   �  �   7  &   �     �     �  .         :      A      ^   
   l      w   j   �      �      �       !     !     !  	   4!     >!  A   V!     �!  !   �!  #   �!     �!     �!     "     $"  3   <"     p"     �"     �"     �"      �"     �"  .   �"     #     %#     8#     O#     i#     q#     ~#     �#  *   �#     �#     �#  S   �#  P   6$     �$  
   �$  '   �$  >   �$     �$  -   %  /   A%     q%  0   �%  4   �%  '   �%     &     &     +&     F&  )   [&  0   �&  %   �&     �&     �&  $   �&     '  
   8'     C'     U'     k'     �'     �'     �'  
   �'  =   �'  >   �'  `   2(  �   �(  -   !)  w   O)  m   �)  ,   5*     b*  !   h*     �*     �*     �*  2   �*      �*  =   +  *   E+  3   p+     �+     �+     �+     �+     �+     �+  _   �+  "   N,     q,  U   ~,  +   �,  ,    -     --     2-     R-     d-     }-  D   �-     �-     �-  "   �-  �  .  @   �/      0     00  r   �0     #1     01  )   =1     g1     t1  H   �1     �1     �1     �1     2  ?    2  -   `2  :   �2  W   �2     !3     53     O3     g3     �3  H   �3     �3     �3  N   �3  a   >4  "   �4  [   �4  %   5  A   E5  1   �5     �5     �5  -   �5  R  6  #   o7  0   �7     �7  5   �7  ?   8     U8  ?   g8  "   �8  L   �8     9     09  	   =9     G9  "   N9     q9     y9  '   �9  $   �9     �9     �9     :  &   4:  
   [:     f:     �:     �:     �:  ,   �:  L   ;  !   O;  .   q;  .   �;  /   �;     �;  �   <     �<     �<  
   �<     �<     �<     �<     �<     �<     =     =     =  P   6=  :   �=     �=     �=     �=     �=      >  1   >  #   J>     n>  
   �>  Q   �>  ^   �>  �   <?  $   �?     �?     @  1   @  	   M@  '   W@     @     �@     �@  q   �@     4A     :A     AA     NA     ]A     zA     �A  M   �A     �A  )   B  (   /B     XB     `B     nB     �B  8   �B  "   �B      C     C     /C  *   HC     sC  J   �C     �C     �C     �C  $   D  	   1D     ;D     LD     UD  '   mD     �D     �D  O   �D  P   E     WE     ^E  2   jE  J   �E     �E  5   �E  7   5F     mF  >   �F  A   �F  .   G     ;G     HG     _G  (   {G  4   �G  2   �G  8   H     EH  
   _H  .   jH  ,   �H     �H     �H     �H     I     *I     2I     CI     TI  :   \I  <   �I  h   �I  �   =J  C   �J  v   $K  n   �K  3   
L     >L  (   EL  '   nL     �L     �L  J   �L  (   �L  N   M  5   kM  E   �M     �M     �M     �M  
   N     N  (   -N  �   VN  &   �N     �N  a   O  &   rO  .   �O     �O     �O     �O     P     P  N   >P     �P     �P  %   �P     4           �              �   �               �   l   Z   �   (           �       �              �       K   Q   �       �       �           �      _          s   [   �           %   �              z   }   3   5   r   G      �   �           S                  ;   �   2   Y               �   �   :   +   y   k   �   $           �           	   f   {         `              �   �   �   n   !         �   �   �   �   8   C   |   W      ?       �              �               �          �   D       �   �   a      >           �   <   �   �   p   U   �   \   �   �   �       F   �   A   �   �   N   #           L   �           P   �       e   J      O              �   �   �   M   1   o   
          �   g   �   j   X   6   '       �                                    �   �       �   ]       �   "   �   c      7   ,   )   &   �   I   �       0   x   �   b   �   R       V               �      @   �   ~       T   �      *       �   �       /   �   B   -   �   �   i   �   �              �          m   h   �       u   �   �   E   d          =      9   ^       �   �   �   �       �   q   �   �   t   �       H       �   �   w   v   �   .    

New releases fix bugs and offer new features.  Do you want to enable this feature? "ffmpeg", "ffprobe" and "ffplay" are required. Complete all
the text boxes below by clicking on the respective buttons. '{}' is not installed on your computer. Install it or indicate another location by clicking the 'Locate' button. ...Finished < Previous A new release is available - v.{0}
 Abort Add metadata to file An item must be selected in the URLs checklist Appearance Application Language Application preferences Apply Are you sure you want to clear the selected log file? Are you sure you want to exit? Assign optional suffix to output file names: At least one "Format Code" must be checked for each URL selected in green. At the bottom of window At the left of window At the right of window At the top of window (default) Audio Codec Auto-create subfolders when downloading playlists Auto-detection Back By assigning an additional suffix you could avoid overwriting files Changes will take effect once the program has been restarted.

Do you want to exit the application now? Check for newer version Check the current output or read the related log file for more information. Checking for newer version Choose a new destination for the files to be trashed Choose a temporary destination for downloads Choose the {} executable Clear selected log Clear the cache when exiting the application Click on "Playlist Items" column to specify indices of the videos in the playlist separated by commas like: "1,2,5,8" if you want to download videos indexed 1, 2, 5, 8 in the playlist.

You can specify range: "1-3,7,10-13" it will download the videos at index 1, 2, 3, 7, 10, 11, 12 and 13.
 Click the "Next" button Click the "Next" button to get started Configuration folder Congratulation! You are already using the latest version.
 Cross-platform graphical interface for FFmpeg and youtube-dl.
 Current path: Default destination folders successfully restored Delete all text from the list Delete the contents of the log files when exiting the application Destination folder Disabled Donation Done! Don’t check SSL certificate Download Download Audio only Download all available subtitles Download all videos in playlist Download by format code Download split audio and video Download subtitles only Download videos by resolution Downloader Downloader preferences Downloads folder	Ctrl+D ERROR: Invalid URL: "{}" ERROR: Invalid option ERROR: Some equal URLs found ERROR: Unable to get format codes.

Unsupported URL:
'{0}' Embed thumbnail in audio file Enable another location to run FFmpeg Enable another location to run FFplay Enable another location to run FFprobe Enter URLs below Enter only alphanumeric characters. You can also use the hyphen ("-") and the underscore ("_"). Spaces are not allowed. Exit Exit	Ctrl+Q Extension FFmpeg FFmpeg logging levels Fatal Error ! File File Preferences Finish Format Code Get Latest Version Get version about your operating system, version of Python and wxPython. Get your files at the destination you specified Go to the next panel Go to the previous panel Goto Help Help viewer How to install pip on your Linux distribution How to upgrade a Python package Icon size: Icon themes If FFmpeg is not on your computer, this application will be unusable If you have already installed FFmpeg on your operating
system, click the "Auto-detection" button. If you want to use a version of FFmpeg located on your
filesystem but not installed on your operating system,
click the "Locate" button. Include the video ID
in the file names Interrupted Process ! Issue tracker Keeps track of the output for debugging errors Locate Locating FFmpeg executables
 Log file list Log folder Log messages Make sure you are using the latest available version of
'{}'. This allows you to avoid download problems.
 Miscellanea Next Next > Not Installed Not everything was successful. Not found OK: Indexes to download Only one video can be played at a time.

Unsupported '{0}':
'{1}' Open temporary downloads Open the default downloads folder Open the temporary downloads folder Options Other options Output Monitor	Shift+O Path to the executables Permission denied: {}

Check execution permissions. Place the toolbar Play selected url Playlist Editor Playlist Items Playlist video items to download Please confirm Please take a moment to set up the application Precompiled Videos Preferences	Ctrl+P Preferred video format Prevent overwriting files Preview Process log: Quality Re-start is required Read and write useful notes and reminders. Ready Refresh all log files Remember that you can always change these settings later, through the Setup dialog. Required: Microsoft Visual C++ 2010 Service Pack 1 Redistributable Package (x86) Reset Resolution Restore the default destination folders Restore the default folders for file conversions and downloads Restrict file names Save all downloads to this temporary location Save each file in the same folder as input file Select a log file Set a persistent location to save exported files Set a persistent location to save the file downloads Set up a temporary folder for downloads Settings Show Logs	Ctrl+L Show the latest version... Showing log messages Shows download statistics and information Shows the latest version available on github.com Shows the text in the toolbar buttons Shows the version in use Size Some text boxes are still incomplete Sorry, all task failed ! Statistics Statistics viewer Stops current process Successfully completed ! System System version TITLE SELECTION Thank You! The URLs contain channels. Are you sure you want to continue? The URLs contain playlists. Are you sure you want to continue? The chosen icon theme will only change the icons,
background and foreground of some text fields. The following settings affect output messages and
the log messages during transcoding processes.
Change only if you know what you are doing.
 The process was stopped due to a fatal error. There are still processes running.. if you want to stop them, use the "Abort" button.

Do you want to kill application? This feature allows you to download video and audio from
many sites, including YouTube.com and even Facebook. Threads used for transcoding (from 0 to 32): Title To exit click the "Finish" button Toolbar customization Translation... URL URL list changed, please check the settings again. URLs have no playlist references Unable to get format codes on '{0}'

Unsupported '{0}':
'{1}' Unexpected error while creating file:

{0} Unexpected error while deleting file contents:

{0} Url Version in Use Video Codec View Viewing last log Viewing log messages WARNING: The selected URL does not refer to a playlist. Only lines marked green can be indexed. Wait....
Retrieving required data. Warn on exit When not available, the chosen video resolution will be replaced with the closest one Where do you prefer to save your downloads? Where do you prefer to save your transcodes? Wiki Wizard completed successfully!
 Work Notes	Ctrl+N Write subtitles to video You are using '{0}' version {1} You are using a development version that has not yet been released!
 fps please try again. {0}: Latest version available: {1} Project-Id-Version: Vidtuber 1.0.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-16 13:08+0100
Last-Translator: Gianluca (jeanslack) Pernigotto <jeanlucperni@gmail.com>
Language-Team: Italian <>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
X-Poedit-SourceCharset: UTF-8
 

Le nuove versioni risolvono bug e offrono nuove funzionalità.  Vuoi abilitare questa funzione? Sono richiesti "ffmpeg", "ffprobe" ed " ffplay". Completa tutti
i campi di testo sottostanti cliccando sui rispettivi pulsanti. "{}" non è installato sul tuo computer. Installalo o indica un'altra posizione cliccando il pulsante "Individua". ...Terminato < Precedente È disponibile un nuovo rilascio - v.{0}
 Interrompere Aggiungi i meta dati al file È necessario selezionare un elemento nell'elenco di controllo degli URL Aspetto Lingua dell'applicazione Preferenze dell'applicazione Applica Sei sicuro di voler cancellare il file di registro selezionato? Sei sicuro di voler uscire dall'applicazione? Assegna un suffisso aggiuntivo ai nomi dei file in uscita: È necessario spuntare almeno un "Codice Formato" per ciascun URL selezionato in verde. Sotto alla finestra A sinistra della finestra A destra della finestra Sopra alla finestra (default) Codec Audio Crea automaticamente le sottocartelle durante il download delle playlist Auto-rileva Indietro Assegnando un suffisso aggiuntivo è possibile evitare di sovrascrivere i file Le modifiche avranno effetto una volta riavviato il programma

Vuoi uscire dall'applicazione ora? Controlla la versione più recente Controlla l'output attuale o leggi il relativo file di registro per ulteriori informazioni. Controllo della versione più recente Scegli una nuova cartella di destinazione per i file da cestinare Scegli una destinazione temporanea per i download Scegli l'eseguibile {} Cancella registro selezionato Svuota la cache quando esci dall'applicazione Fare clic sulla colonna "Contenuto della playlist" per specificare gli indici dei video nella playlist separati da virgole, esempio: "1,2,5,8" se si desidera scaricare i video indicizzati 1, 2, 5, 8 nella playlist.

È possibile specificare l'intervallo: "1-3,7,10-13" il quale scaricherà i video all'indice 1, 2, 3, 7, 10, 11, 12 e 13.
 Fare clic sul pulsante "Successivo" Fare clic sul pulsante "Successivo" per iniziare Cartella di configurazione Congratulazioni! Stai già usando l'ultima versione.
 Interfaccia grafica multi-piattaforma per FFmpeg e youtube-dl.
 Percorso attuale: Cartelle di destinazione predefinite ripristinate correttamente Elimina tutto il testo dall'elenco Eliminare il contenuto dei file di registro quando si esce dall'applicazione Cartella di destinazione Disabilitato Donazione Fatto! Non controllare il certificato SSL Scarica Scarica solo il file audio Scarica tutti i sottotitoli disponibili Scarica tutti i video
della playlist Scarica per codice formato Scarica audio e video divisi Scarica solo i sottotitoli Scarica video in base alla risoluzione Downloader Preferenze del downloader Cartella dei download	Ctrl+D ERRORE: URL non valido: "{}" ERRORE: Opzione non valida ERRORE: sono stati trovati alcuni URL uguali ERRORE: Impossibile ottenere i codici di formato.

URL non supportato:
'{0}' Aggiungi miniatura
nel file audio Abilita un'altra posizione per eseguire FFmpeg Abilita un'altra posizione per eseguire FFplay Abilita un'altra posizione per eseguire FFprobe Aggiungi gli URL qui sotto Immettere solo caratteri alfanumerici. È inoltre possibile utilizzare il trattino ("-") e il trattino basso ("_"). Non sono ammessi spazi vuoti. Esci Esci	Ctrl+Q Estensione FFmpeg Livelli logs FFmpeg Errore Fatale ! File Preferenze file Fine Codice Formato Ottieni l'ultima versione Ottieni la versione del tuo sistema operativo, la versione di Python e wxPython. Ottieni i tuoi file nella destinazione che hai specificato Vai al prossimo pannello Vai al pannello precedente Vai Aiuto Visualizzatore di aiuto Come installare pip sulla tua distribuzione Linux Come aggiornare un pacchetto Python Dimensioni icona: Temi icona Se FFmpeg non si trova sul tuo computer, questa applicazione sarà inutilizzabile Se hai già installato FFmpeg sul tuo sistema operativo, fai click
sul pulsante "Auto-rileva". Se vuoi usare una versione di FFmpeg situata sul tuo filesystem
ma non installata sul tuo sistema operativo, fai clic sul pulsante
"Individua". Includi l'ID video nei
nomi del file Processo Interrotto ! Segnala un Problema Tiene traccia dell'output per gli errori di debug Individua Individuazione degli eseguibili FFmpeg
 Elenco file di registro Cartella dei registri Messaggi di registro Assicurati di utilizzare l'ultima versione disponibile di '{}'
Ciò ti consente di evitare problemi di download.
 Varie Avanti Successivo > Non installato Non tutto ha avuto successo. Non trovato OK: Indici da scaricare È possibile riprodurre un solo video alla volta

'{0}' non supportato:
'{1}' Apri download temporanei Apri la cartella dei download predefinita Apri la cartella dei download temporanei Opzioni Altre opzioni Monitoraggio Output	Shift+O Percorso degli eseguibili Permesso negato: {}

Controlla i permessi di esecuzione. Posiziona la barra degli strumenti Riproduci l'URL selezionato Editor di Playlist Contenuto della playlist Elementi video della playlist da scaricare Prego confermare Si prega di dedicare qualche istante alla configurazione dell'applicazione Video Precompilati Preferenze	Ctrl+P Formato video preferito Impedisci la sovrascrittura
dei file Anteprima Log di processo: Qualità È necessario riavviare Leggi e scrivi utili note e promemoria. Pronto Aggiorna tutti i registri Ricorda che puoi sempre modificare queste impostazioni con il dialogo di Setup. Richiede: Microsoft Visual C++ 2010 Service Pack 1 Redistributable Package (x86) Azzera Risoluzione Ripristina le cartelle di destinazione predefinite Ripristina le cartelle predefinite per le conversioni e i download di file Limita i nomi dei file Salva tutti i download in questa posizione temporanea Salva ogni file nella stessa cartella del file di input Seleziona un file di registro Imposta una posizione persistente per salvare i file esportati Imposta una posizione persistente per salvare i download dei file Imposta una cartella temporanea per i download Impostazioni Mostra Registri	Ctrl+L Mostra l'ultima versione... Visualizzazione dei messaggi di registro Mostra le statistiche e le informazioni sui download Mostra l'ultima versione disponibile su github.com Mostra il testo nei pulsanti della barra degli strumenti Mostra la versione in uso Dimensione Alcune caselle di testo sono ancora incomplete Spiacente, tutte le attività sono fallite ! Statistiche Visualizzatore di statistiche Interrompe il processo corrente Completato con successo ! Sistema Versione Sistema SELEZIONE TITOLO Grazie! Gli URL contengono canali. Sei sicuro di voler continuare? Gli URL contengono playlist. Sei sicuro di voler continuare? Il tema delle icone scelto cambierà solo le icone,
lo sfondo e il primo piano di alcuni campi di testo. Le seguenti impostazioni influenzano i messaggi di output
e i messaggi di registro durante i processi di transcodifica.
Modificale solo se sai cosa stai facendo.
 Il processo è stato interrotto a causa di un errore irreversibile. Ci sono ancora processi avviati... Se vuoi interromperli usa il bottone "Interrompere".

Vuoi uccidere l'applicazione? Questa funzione ti consente di scaricare video e audio da 
molti siti, inclusi YouTube.com e persino Facebook. Thread utilizzati per la transcodifica (da 0 a 32): Titolo Per uscire fare clic sul pulsante "Fine" Personalizzazione barra degli strumenti Traduzione... URL Elenco URL modificato, si prega di controllare nuovamente le impostazioni. Gli URL non hanno riferimenti a playlist Impossibile ottenere i codici di formato su '{0}'

'{0}' Non supportato:
'{1}' Errore imprevisto durante la creazione del file:

{0} Errore imprevisto durante l'eliminazione del contenuto del file:

{0} Url Versione in uso Codec Video Visualizza Visualizzazione ultimo log Visualizzazione dei messaggi di registro ATTENZIONE: L'URL selezionato non fa riferimento a una playlist. È possibile indicizzare solo le righe contrassegnate in verde. Attendi....
Recupero i dati richiesti. Avvisa all'uscita Quando non disponibile, la risoluzione video selezionata verrà sostituita con quella più vicina Dove desideri salvare i tuoi download? Dove preferisci salvare le tue transcodifiche? Wiki Tutto completato con successo!
 Note di Lavoro	Ctrl+N Scrivi sottotitoli al video Stai usando '{0}' versione {1} Stai utilizzando una versione di sviluppo che non è ancora stata rilasciata!
 fps riprova. {0}: Ultima versione disponibile: {1} 