��    �     �  �  �-      �<  /   �<     �<     =     %=  ,   <=  $   i=  �   �=  Q   �>  ]   �>  w   5?  �   �?  2   t@  p   �@     A     $A     3A     9A     EA  
   RA     ]A     oA     �A  B   �A  !   �A  &   �A  #   B  n   >B     �B  2   �B  :   �B     0C     HC     WC     ]C  �   mC  i   �C  �   ZD     �D     �D  	   E     E  ;   E     TE     iE     ~E  4   �E  +   �E  /   �E  .    F  
   OF     ZF     rF     xF  #   �F  5   �F  b   �F     MG  r   lG     �G  ,   �G  J   H  9   dH     �H     �H     �H     �H     I     I     I     "I     0I     AI     OI  3   eI     �I  E   �I  J   �I  1   BJ     tJ     �J  �   �J     8K  �   =K  y   �K    9L  	   PM  C   ZM     �M     �M  X   �M  &   N     =N  :   DN  g   N     �N     �N  O   O  5   eO  K   �O     �O     P  %   P  +   AP  4   mP  %   �P  .   �P  ,   �P     $Q  �   ?Q     R     R     :R  ,   MR  #  zR     �S  &   �S     �S     �S     �S     T  J   *T     uT  :   �T  h   �T  k   .U  )   �U  
   �U     �U     �U     �U     V     V  7   /V  #   gV     �V     �V     �V  e   �V  l   ,W     �W     �W     �W     �W     �W  >   �W     #X     1X  	   EX     OX     [X     lX  1   uX     �X     �X  0   �X  b   �X     XY     xY     �Y     �Y     �Y     �Y  .   �Y     Z  A   6Z     xZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z  4   �Z     4[  )   =[  (   g[     �[  2   �[     �[     �[     �[     \      \     )\  8   =\      v\     �\     �\     �\     �\     ]     &]     B]  
   `]     k]     �]     �]  	   �]  "   �]     �]  	   �]     �]      ^     ^     ^  !   5^     W^  @   t^  :   �^  I   �^  h   :_     �_     �_     �_     �_      `     `  %   (`  %   N`  &   t`     �`     �`     �`     �`  �   �`     �a     �a     �a     �a  w   �a     8b     Tb     Yb     eb  $   sb     �b     �b  	   �b  G   �b     c      c     5c     Ec     Zc     jc     ~c     �c     �c     �c     �c     �c     �c     �c  	   d     d  &   d     Cd  3   _d  	   �d  I   �d  �   �d  z   �e     "f  "   )f  "   Lf  +   of     �f     �f     �f  
   �f  	   �f  /   �f  >   g  @   Kg     �g     �g  �   �g     =h     Th  &   sh  )   �h  "   �h     �h  /   �h  o   *i  !   �i  %   �i  (   �i     j  !   "j  #   Dj  $   hj     �j     �j     �j     �j     �j     �j     �j     �j  -   �j     !k  
   Ak     Lk  D   Xk  )   �k  D   �k  a   l  �   nl  �   Xm     �m  D   �m  8   2n  @   kn     �n  -   �n     �n     
o     o     %o  &   8o     _o     ~o     �o  H   �o  	   �o      �o     p     &p  �   9p  3   q  %   Rq     xq  .   �q  .   �q     �q  �   �q  	   �r     �r  
   �r     �r     �r     �r  
   �r     �r  B   �r  j   1s     �s     �s  =   �s     �s      t  
   t     t      t  ;   ,t  H   ht  7   �t     �t     u  .   u     Hu     Tu     Yu     ]u     pu     uu     |u     �u     �u  �   �u     =v     Qv     _v     mv  	   �v  	   �v     �v     �v     �v  B   �v     w  S   w  A   hw     �w     �w     �w     �w     �w  ,   x  !   Ex  (   gx     �x  #   �x  *   �x     �x  +   y  (   2y  )   [y     �y     �y     �y     �y     �y  E   �y     z  $   0z  =   Uz     �z     �z  
   �z     �z  3   �z     {     ({  .   -{     \{  "   n{  )   �{     �{     �{      �{     �{     
|  .   |     H|     g|     z|     �|     �|     �|     �|     �|     �|     �|     �|     }      }     .}     6}  %   C}     i}     r}     z}  �   �}  @   3~  �   t~  G  K     ��     ��  *   Ā     �  
   ��      �     �  S   �     q�  '   x�  3   ��  :   ԁ  P   �  %   `�  &   ��     ��     ��     ��  
   ͂     ؂     ��  '   �  >   �     V�     j�  &   v�     ��     ��     ǃ     ߃     �     ��  4   ��  -   4�  /   b�     ��     ��  !   ��     ք  #   �     �  (   (�  �   Q�  C   �  _   -�  0   ��  4   ��  �   �     ��     ��  e   Ň     +�  w   B�  [  ��  y   �  �   ��  G  �  u   g�  �   ݌  �   a�  (   �  �  /�  Y     )   �  '   F�  7   n�  /   ��     ֐     �     �  `   ��  '   _�  1   ��     ��     ʑ     ݑ     �  @   �     G�  #   \�  #   ��  )   ��  0   Β  %   ��     %�  4   >�     s�  $   x�     ��  <   ��     �  5   �  3   G�  P   {�  (   ̔  Z   ��  �   P�     
�  �   (�  $   �  
   �  
   �     *�     <�     N�     h�     ~�     ��     ��     ��     ��     ԗ      �     �  9   $�     ^�     u�     |�     ��  
   ��  =   ��  >   �  P  "�     s�  `   �  3   T�  '   ��  p   ��  �   !�  )   ��  $   ٜ  2   ��  -   1�  C   _�  n   ��  Q   �  X   d�  w   ��  $   5�  m   Z�  /   ȟ  �   ��  s   |�  ,   �     �     0�  !   6�     X�     n�     t�     ��     ��     ��     ��     ��  ^   ��     �  2    �      S�  =   t�  )   ��  X   ܢ  *   5�  3   `�     ��     ��     ��     £     ڣ  ,   ޣ     �  
   �     "�  
   1�     <�     B�     N�     [�     i�     w�     ��     ��  #   ��  	   Ф     ڤ     ��     �  "   0�     S�     j�     ��     ��     ��     ҥ     �     �  D    �  E   e�  ,   ��  v   ئ     O�     i�     n�     �     ��     ��  _   ��     �  "   2�  (   U�     ~�     ��     ��      ��  e   ɨ  �   /�  h   �  U   R�  +   ��  ,   Ԫ     �     �     *�     0�     7�     <�     \�     n�     ��  D   ��     �     ��  @   �  %   [�      ��  ^   ��  K   �  &   M�     t�     ��  &   ��     ȭ     ԭ     ڭ     �     �     ��  �   �     ��      �  �   �  #   ��     ��  �   ů     v�  G   ~�     ư     ذ     ��     
�  x   �  �   ��  &   �  #   ?�  !   c�     ��     ��     ��     β     �  ~   �     ��     ��     ��  N   ��  "   �  ,   .�     [�  l  l�  B   ٵ     �     8�     J�  /   _�  (   ��    ��  [   з  f   ,�  u   ��  �   	�  .   �  i   �     �     ��     ��  
   ��  
   ��  
   ��     ĺ     ݺ     �  ;    �  "   <�  '   _�  )   ��  �   ��     =�  >   W�  <   ��     Ӽ     ��     ��     �  �   �  ^   ��  �   ��     ��     ��     ��     ��  B   Ⱦ     �     (�     ?�  .   R�  &   ��  (   ��  '   ѿ  
   ��     �     #�     +�  &   F�  .   m�  N   ��     ��  p   �     v�  4   ��  H   ��  B   	�     L�     i�     ��  -   ��     ��     ��     ��     ��     
�     �     /�  A   H�     ��  X   ��  J   �  7   N�     ��     ��  �   ��     R�  �   [�  �   ��  ;  q�     ��  <   ��     ��     �  X   %�  3   ~�     ��  K   ��  L   �     R�     k�  U   ��  ?   ��  H   �  "   b�     ��  1   ��  5   ��  !   �  "   -�  /   P�  ,   ��     ��  �   ��     ��  %   ��  (   ��  (   ��  G  %�     m�  +   ��     ��  	   ��     ��     ��  Z   �     j�  B   ��  �   ��  {   K�  7   ��     ��     �  	   *�     4�     L�     _�  ;   r�  $   ��     ��     ��     ��  �   �  z   ��     
�     �     ,�     ;�     D�  ;   `�     ��     ��     ��     ��     ��     ��  9   �     B�     [�  8   j�  p   ��  "   �  !   7�     Y�     h�     ��     ��  '   ��      ��  5   ��     "�     =�     M�     h�     t�     ��     ��     ��  8   ��     ��  =   ��  .   6�     e�  =   ��  "   ��  
   ��     ��     ��  	   �      �  8   5�  *   n�  7   ��      ��  $   ��  $   �     <�  $   V�  &   {�     ��  #   ��     ��  "   ��  	   �  %   �  	   <�  
   F�     Q�     ]�     c�     }�  !   ��  $   ��  Q   ��  H   .�  Q   w�  n   ��     8�     ?�     ]�  &   x�     ��     ��  (   ��  (   ��  )    �     J�     b�     z�     ��  �   ��  
   =�     H�     V�  "   r�  t   ��     
�     '�     -�     :�  .   K�     z�  '   ��  
   ��  O   ��     �     �     3�     J�     c�     x�     ��     ��     ��     ��     ��     �     �     -�     5�     G�  !   _�     ��  A   ��     ��  P   ��  �   @�  q   .�  	   ��  #   ��  #   ��  .   ��     !�     7�     J�     ^�     m�  8   ~�  C   ��  K   ��     G�  "   Z�  �   }�     �      �  3   @�  )   t�  %   ��     ��  .   ��  d   �  *   r�  &   ��  &   ��     ��  (   �  *   *�      U�     v�     ��     ��     ��     ��     ��     ��     ��  +   ��  $   �     7�     I�  ;   Y�  4   ��  F   ��  V   �  	  h�  �   r�     �  I   �  M   f�  J   ��     ��  0   �  !   F�     h�     w�     ��  0   ��  "   ��     ��     
�  H   �     a�  #   m�     ��     ��  �   ��  G   ��  (   ��     �  8   (�  8   a�     ��  �   ��     A�     J�     Q�  	   _�  "   i�     ��     ��     ��  L   ��  ^   !�     ��     ��  F   ��     ��     ��     �     �     !�  B   -�  O   p�  >   ��     ��     �  @   <�     }�     ��     ��     ��  	   ��     ��  	   ��     ��      ��  �   
�     ��     ��     ��     ��     ��  	   ��  (   �     0�     8�  G   L�     ��  L   ��  F   ��     0�     J�     a�     z�     ��  (   ��  ,   ��  /   	�     9�  %   T�  (   z�     ��  -   ��  /   ��  4   �     F�     U�     ^�     m�     ��  E   ��     ��  $   ��  I   !�     k�  )   ~�     ��     ��  5   ��     	�  
   )�  :   4�     o�  +   ��  5   ��      ��     �  (   $�     M�     `�  "   s�      ��     ��     ��     ��     ��     �     
�      #�     D�     c�     q�     ��     ��     ��     ��     ��     ��     ��  &   ��  �   $�  W   ��    H�  �  f  '       > $   V    {    �    �    � `   �     +    9   J @   � B   � (    )   1 	   [    e    s    �    �    � ,   � B   � !   )    K 8   Z    �    �    � 	   � 	   � 
   � E   � 4   E ,   z    � 
   � *   �    � $       9 *   Y �   � >   U f   � G   � =   C	 �   �	 $   3
 -   X
 w   �
    �
     k  � �    �   � h  + r   � �    �   � 1   e �  � Y   [ (   � %   � D    ?   I    �    �    � ]   � +   $ ;   P    �    �    �    � F   �    2 0   Q .   � 1   � 0   � 8       M C   e    � )   �    � D   � '   8 9   ` /   � Q   � )    q   F �   � "   � �   � (   �    �    �    �    � $       * 	   A    K    a    u    �    �     �    � :   �    6    O    W    l 
   | 9   � I   � f   �   r Z   � A   R  -   �  g   �  �   *! $   �!    �! .   " &   F" F   m" n   �" ]   ## d   �# n   �# !   U$ �   w$ 5   �$ t   1% ~   �% ,   %&    R&    o& )   v& "   �&    �&    �&    �&    �&    
'    !' 	   )' Y   3'    �' =   �' '   �' N   �' :   F( o   �( '   �( 5   ) 	   O)    Y)    l)    �)    �) 6   �)    �)    �)    �)    *    *     *    /*    >*    S*    d*    s*    �* "   �* 	   �*    �* $   �*    + #   3+    W+    o+ $   �+    �+    �+ '   �+    ,    5, 1   L, 6   ~, .   �, z   �,    _-    }-    �-    �-    �-    �- i   �- %   M. -   s. ,   �.    �.    �.    �. '   / U   +/ �   �/ S   :0 T   �0 '   �0 (   1    41    R1    h1    n1    u1 #   z1    �1    �1 "   �1 .   �1    &2    92 G   T2 1   �2 $   �2 l   �2 O   `3 -   �3    �3    �3     4    -4    :4    C4    K4    O4    g4 �   z4 *   `5 )   �5 �   �5    o6    �6 �   �6    e7 P   n7    �7 '   �7    8    8 �   38 �   �8 '   M9 1   u9 .   �9 $   �9     �9    : %   8:    ^: �   }:    ;     ;    '; R   :; .   �; .   �;    �;    	  �      �              �      �       �  �  �       �  �   `   �                    *  �  c  ]   �  �          R  a       )  p   �   �   M  �      �       �        Z   g      �      �   �  �      N  @   �     I      o   �       �      �   ?  �  �   \       Z      X      �     +  �      �  �  �  �   �  �  <          H    �   }   �       g   �   ;   �  �   �    /  �      �           |   .  �   �   e           b   �          &  d      �   �  �            �   �              �  �   �       �  T   n          �   �    �      s  j      �  �  7  K      �  :  �  �     y  �   �  ,  �  �   �          �      !  �      e  #          �  P       �   U           �  {  �  �   �  �          �  �   u  +             F   �      �          �  �       j   �  �               �     �  z   �   N       �  z      �               �   Y      �      �  �  	  p  )   n   �   �  �       ~   �        =      �   �      �          v  S      �   �  �   �   �  K      �   �           �   H   �  �  [   C    �  +       �   E      �  �  �   P      �                �  {  �  =   �   4   "          w     Q   G   6   *  Y  �  M         �   �   P      �  �          �  �  �  %        '  �   0  �  �  ^        �   G     R      I  �       �      8        �  �  J           �  �           �  �      h   F  �      i       �      �   I   B  Q  �         �  C  f  �          0   �  k     v      ~  �  C       �      �  �      t  �       �         %  _   >  �     �          �  �   �       �          �            �  r  �      �  0             �      �      �   $   �   7      T      z  t     �  �                     >   �  r      �   h    �   d       �  �  �  h      �       �  �   !  q  Q  �  �  �      �  V  '      �   O   �  �  4      f   \  �   �      u            �      �  x  :      �      >  1  �  5  �   |     5  s       �     1         p  =  3                 ~  A      m   �   :       �        U  �  �       �  -      �  �      -   �   Z  �  �     @  �            �  �     �   <   �  .   c  �     �   l      
  �            �  �  �       2      �      t   S    �  |      O      9      �      �  4        �       �              "  �   �          �     �  �        �  �        6    @         �    7       �  &      �  d  w   [  �      *                 8  $      G  �         F  �          \  ]  �   D              �   X   ?      u  L    �      �  �  �  �                  �  L   �            �   �  �  ;  �             �  c   ^   "   S       �  V       J       �   �      M  �     U  e        ]  m  Y   l  �   �  3   �  ;  �   �  s  �   �  �   �   �   `  �  �  �  �           (      �  [  �  #      �   �  y  E   b  �   �  �  a  N      %   k    /   �     )  �  �  i    l           _  �  �           !     
      k   .    �  �  �      O  D   m  W  R   �  v   �      �   y   T  �  B       �  '   W  x   B     _  �   }  �  q  f  �  �    q   	       $      &   
   �      X  �          �  �      �        L  �  �  w  ?   �  �  H  �   �       �      �   �   i  ,       9   (   �  �   �       r  �     9  A   �       �  K       (      �      J  �   �  �  n  j  6  �       �  �   �   ^  �  -  �   b        `          �  3         }  �  �  8   x          V      <  �      �   1  /      5   D  �   2  �       �      �   {           �    �   ,  2   �  g  W           o  �      �      �     #   �   A    �       �  E  �     �  �  �  �  a  o  �       �    

New releases fix bugs and offer new features. 

This is Videomass v.{0}

 
  ...Not found 
  ..Nothing available 
First, choose a topic in the drop down list  Do you want to enable this feature? "Auto" keeps all audio stream but processes only the one of the selected index; "All" keeps all audio streams and processes them all with the properties of the selected index; "Index only" processes and keeps only the selected index audio stream. "Video" to save the output file as a video; "Audio" to save as an audio file only "cpu-used" sets how efficient the compression will be. The meaning depends on the mode above. "ffmpeg", "ffprobe" and "ffplay" are required. Complete all
the text boxes below by clicking on the respective buttons. "good" is the default and recommended for most applications; "best" is recommended if you have lots of time and want the best compression efficiency; "realtime" is recommended for live/fast encoding 'Successful!

A new empty preset has been created. '{}' is not installed on your computer. Install it or indicate another location by clicking the 'Locate' button. ...Finished ...Interrupted 180° 90° (left) 90° (right) < Previous =  Below max peak =  Clipped peaks =  No changes A file with this name already exists, do you want to overwrite it? A long description of the profile A new preset was successfully imported A new release is available - v.{0}
 A set of useful tools for audio and video conversions. Save your profiles and reuse them with Presets Manager. A short profile name A target file must be selected in the queued files A useful tool to search for FFmpeg help topics and options A/V Conversions	Shift+V AV-Conversions Abort About Videomass Activate RMS-based normalization, which according to mean volume calculates the amount of gain to reach same average power signal. Activate peak level normalization, which will produce a maximum peak level equal to the set target level. Activate two passes normalization. It Normalizes the perceived loudness using the "​loudnorm" filter, which implements the EBU R128 algorithm. Activates row-mt 1 Add Add Files Add Profile Add a new profile from this panel with the current settings Add metadata to file Additional arguments Advanced Options All default presets have been successfully recovered All presets have been exported successfully Already exist: 

{}

Do you want to overwrite?  An item must be selected in the URLs checklist Appearance Application preferences Apply Apply Denoisers Filters Are you sure to empty trash folder? Are you sure you want to clear the selected log file? Are you sure you want to delete the selected profile? It will no longer be possible to recover it. Are you sure you want to exit? Are you sure you want to remove "{}" preset?

 It will be moved to the "Removals" subfolder of the presets folder. Aspect Ratio Assign optional suffix to output file names: At least one "Format Code" must be checked for each URL selected in green. At least two files are required to perform concatenation. At the bottom of window At the left of window At the right of window At the top of window (default) Audio Audio Codec Audio Encoder Audio Filters Audio Properties Audio Streams Audio Streams Mapping Audio normalization is required only for some files Audio normalization off Audio normalization processes cannot be saved on the Presets Manager. Audio normalization will not be applied because the source signal is equal Auto-create subfolders when downloading playlists Auto-detection Auto-exit after playback Available video codecs. "Copy" is not a codec but indicate that the video stream is not to be re-encoded and allows changing the format or other parameters Back Be careful! The selected preset will be overwritten with the default one. Your profiles may be deleted!

Do you want to continue? Be careful! This action will restore all presets to default ones. Your profiles may be deleted!

Do you want to continue? Bit depth is the number of bits of information in each sample, and it directly corresponds to the resolution of each sample. Bit depth is only meaningful in reference to a PCM digital signal. Non-PCM formats, such as lossy compression formats, do not have associated bit depths. Box Color By assigning an additional suffix you could avoid overwriting files CODING CAPABILITY Cache folder Cannot continue: The duration in the timeline exceeds the duration of some queued files. Cannot save current data in file '{}'. Center Change the size and color of the timestamp during playback Changes will take effect once the program has been restarted.

Do you want to exit the application now? Check for new presets Check for newer version Check for the latest Videomass version at <https://pypi.org/project/videomass/> Check new versions of Videomass presets from homepage Check the current output or read the related log file for more information. Checking for newer version Choose a download folder Choose a folder to export all presets Choose a folder to save the selected preset Choose a new destination for the files to be trashed Choose a preset and view its profiles Choose a temporary destination for conversions Choose a temporary destination for downloads Choose a topic in the list Choose an index from the available audio streams. If the source file is a video, it is recommended to select a numeric audio index. If the source file is an audio file, leave this control to "Auto". Choose the {} executable Clear all enabled filters  Clear selected log Clear the cache when exiting the application Click on "Playlist Items" column to specify indices of the videos in the playlist separated by commas like: "1,2,5,8" if you want to download videos indexed 1, 2, 5, 8 in the playlist.

You can specify range: "1-3,7,10-13" it will download the videos at index 1, 2, 3, 7, 10, 11, 12 and 13.
 Click the "Next" button Click the "Next" button to get started Close Videomass Columns: Concatenate Demuxer	Shift+D Concatenate media files Concatenate multiple media files based on import order without re-encoding Configuration folder Congratulation! You are already using the latest version.
 Consider transforms as relative to previous frame if checked, absolute if unchecked. Default is checked. Constant rate factor. Lower values = higher quality and a larger file size. Set -1 to disable this control. Constrain proportions (keep aspect ratio) Container: Conversions folder	Ctrl+C Convert Convert using FFmpeg Create a new preset Create a new profile Create a new profile and save it in the selected preset Create a new profile on "{}" preset Create animated GIF Create thumbnails Create tiled mosaics Create video from a sequence of images, based on import order, with the ability to add an audio file. Create, edit and use quickly your favorite FFmpeg presets and profiles with full formats support and codecs. Crop Filter Crop to height Crop to width Cropping Cropping area selection  Cross-platform graphical interface for FFmpeg and youtube-dl.
 Current path: DECODING CAPABILITY DISABLED: Data Format Deadline/Quality Decoders Default destination folders successfully restored Default position Deinterlace Deinterlace the input video with `w3fdif` filter Deinterlace the input video with `yadif` filter. Using FFmpeg, this is the best and fastest choice Deinterlaces with w3fdif filter Deinterlaces with yadif filter Deinterlacing Deinterlacing and Interlacing Delete Delete all files from the list Delete all files in the Videomass Trash folder Delete all text from the list Delete the contents of the log files when exiting the application Delete the selected profile Demuxing only Demuxing/Muxing support Denoiser Denoiser filters Denominator Description Destination folder Directories are not allowed, just add files, please. Disabled Display Aspect Ratio. Set to 0 to disable Display of selected items in text format Displays timestamp Displays timestamp when playing movies with FFplay Don't show this dialog again Donation Done! Don’t check SSL certificate Download Download Audio only Download all Videomass presets locally from the homepage Download all available subtitles Download all videos in playlist Download by format code Download files using youtube-dl Download split audio and video Download subtitles only Download the latest presets Download videos by resolution Downloader Downloader preferences Downloads folder	Ctrl+D Drag one or more files below Duplicate Duplicate files are rejected: > {} Duration Duration: ENABLED: ERROR ERROR: Invalid URL: "{}" ERROR: Invalid option ERROR: Missing audio stream:
"{}" ERROR: Some equal URLs found ERROR: Typing error on JSON keys: {}

File: "{}"
key malformed ? ERROR: Unable to get format codes.

Unsupported URL:
'{0}' ERROR: {0}

{1} is not installed, use your package manager to install it. Easily download videos and audio in different formats and quality from YouTube, Facebook and more sites. Edit Edit profile of the "{}" preset Edit the selected profile Embed thumbnail in audio file Empty Trash Enable a single still image Enable another location to run FFmpeg Enable another location to run FFplay Enable another location to run FFprobe Enable hqdn3d denoiser Enable nlmeans denoiser Enable stabilizer Enable text format Enable virtual tripod mode if checked, which is equivalent to relative=0:smoothing=0. Default is unchecked. Use also tripod option of vidstabdetect. Enabled Encoders Enter URLs below Enter name for new preset Enter only alphanumeric characters. You can also use the hyphen ("-") and the underscore ("_"). Spaces are not allowed. Error, invalid preset: "{}" Exit Exit	Ctrl+Q Export all... Export entire presets folder as copy Export selected Export selected preset as copy Extension Extract images (frames) from your movies in JPG, PNG, BMP, GIF formats. FFmpeg FFmpeg configuration FFmpeg decoders FFmpeg documentation FFmpeg encoders FFmpeg file formats FFmpeg help topics FFmpeg logging levels FFplay FFplay   ...not found ! FFprobe   ...not found ! FILE SELECTION Fatal Error ! File File Name File Preferences File does not exist or is invalid:  %s File does not exist:

"{}"
 File list changed, please check the settings again. File name File to concatenate
Output filename
Destination
Output Format
Time Period File to process
Output filename
Destination Folder
Output Format
Additional arguments
Audio file
Shortest
Resizing
Pre-input
Frame per Second (FPS)
Still image duration
Overall video duration File without format extension: please give an appropriate extension to the file name, example '.mkv', '.avi', '.mp3', etc. Finish First pass of the selected profile First select a profile in the list First, choose a topic in the drop down list Flags disabled Flags enabled Flags settings Font Color Font Size For more details, see the Videomass User Guide: For more information, visit the official FFmpeg documentation: Force original aspect ratio using
padding rather than stretching Format Code Formats must be comma-separated Frames repeat a given number of times per second. In some countries this is 30 for NTSC, other countries (like Italy) use 25 for PAL From Movie to Pictures From Movie to Pictures	Shift+S From an image sequence to a video file Gathers information of multimedia streams Generates duo video for comparison Get Latest Version Get your files at the destination you specified Gets maximum volume and average volume data in dBFS, then calculates the offset amount for audio normalization. Go to the 'A/V Conversions' panel Go to the 'Concatenate Demuxer' panel Go to the 'From Movie to Pictures' panel Go to the 'Home' panel Go to the 'Presets Manager' panel Go to the 'Still Image Maker' panel Go to the 'YouTube Downloader' panel Go to the next panel Go to the previous panel Goto Height Height: Help Help viewer Home panel	Shift+H How to install pip on your Linux distribution How to upgrade a Python package Icon size: Icon themes If FFmpeg is not on your computer, this application will be unusable If activated, hides some output messages. If checked, the FFplay window will auto-close at the end of playback If you have already installed FFmpeg on your operating
system, click the "Auto-detection" button. If you want to keep the aspect ratio, select "Constrain proportions" below and
specify only one dimension, either width or height, and set the other dimension
to -1 or -2 (some codecs require -2, so you should do some testing first). If you want to use a version of FFmpeg located on your
filesystem but not installed on your operating system,
click the "Locate" button. Ignore case Ignore case distinctions: characters with different case will match. Images need to be resized, please use Resizing function. Import a group of presets from a folder and update existing ones Import a new preset Import a new preset or update an existing one Import a new presets folder Import group Import preset Include audio file Include the video ID
in the file names Incomplete profile assignments Index Selection: Informations Integrated Loudness Target in LUFS. From -70.0 to -5.0, default is -24.0 Interlace Interlaces with interlace filter Interrupted Process ! Invalid file: '{}' Invalid preset loaded.
It is recommended to remove it or rewrite it into a JSON format compatible with Videomass.

Possible solution: open the Presets Manager panel, go to the presets column and try to click the "Restore" button Invert transforms if checked. Default is unchecked. Is 'ffmpeg' installed on your system? Issue tracker It can reduce the file size, but takes longer. Keeps track of the output for debugging errors Level Limiter for the maximum peak level or the mean level (when switch to RMS) in dBFS. From -99.0 to +0.0; default for PEAK level is -1.0; default for RMS is -20.0 Listening Load Load Frame Locate Locating FFmpeg executables
 Log file list Log folder Log messages Loudness Range Target in LUFS. From +1.0 to +20.0, default is +7.0 Make sure you are using the latest available version of
'{}'. This allows you to avoid download problems.
 Map: Max volume dBFS Maximum True Peak in dBTP. From -9.0 to +0.0, default is -2.0 Mean volume dBFS Media Streams Media type Media: Miscellanea Move horizontally - set to -1 to center the horizontal axis Move source file to the Videomass trash folder after successful encoding Move vertically - set to -1 to center the vertical axis Multimedia streams analyzer Muxers and Demuxers Muxers and demuxers available for used FFmpeg. Muxing only Name New New size in pixels Next Next > No Audio No files to delete No new version available No presets found.

Possible solution: open the Presets Manager panel, go to the presets column and try to click the "Restore all..." button No such folder '{}' Normalization Not Installed Not everything was successful. Not found Numerator OK: Indexes to download Off Offset dBFS One or more comma-separated format names to include in the profile One-Pass One-Pass, Do not start with `ffmpeg -i filename`; do not end with `output-filename` Only one video can be played at a time.

Unsupported '{0}':
'{1}' Open an audio file Open audio file Open one or more files Open temporary conversions Open temporary downloads Open the Videomass Trash folder if it exists Open the default downloads folder Open the default file conversions folder Open the selected files Open the temporary downloads folder Open the temporary file conversions folder Open...	Ctrl+O Opens the Videomass cache folder, if exists Opens the Videomass configuration folder Opens the Videomass log folder, if exists Optimizations Options Other options Other unofficial resources: Output Format Output Format. Empty to copy format and codec. Do not include the `.` Output Monitor	Shift+O Output folder does not exist:

"{}"
 Output format extension. Leave empty to copy codec and format Output format: PEAK-based volume statistics Parameters Path to the executables Permission denied: {}

Check execution permissions. Place the toolbar Play Play and listen to the result of audio filters Play selected url Play the selected file in the list Play the video until audio track finishes Playlist Editor Playlist Items Playlist video items to download Please Confirm Please confirm Please take a moment to set up the application Post-normalization references: Precompiled Videos Preferences	Ctrl+P Preferred video format Preset Presets Presets Manager Presets Manager	Shift+P Prevent overwriting files Preview Preview video filters Process log: Processing... Profile Profile Name Profile already stored with same name Profiles Quality Quality/Speed
ratio modifier: Queued File
Output Format
Web Optimize
Audio Codec
Audio bit-rate
Audio Channels
Audio Rate
Bit per Sample
Audio Normalization
Time Period
Input Audio Map Queued File
Pass Encoding
Profile Used
Output Format
Time Period Queued File
Web Optimize
Output Format
Video Codec
Aspect Ratio
FPS
Audio Codec
Audio Channels
Audio Rate
Audio bit-rate
Bit per Sample
Audio Normalization
Input Audio Map
Output Audio Map
Subtitles Map
Time Period Queued File
Web Optimize
Pass Encoding
Output Format
Video Codec
Video bit-rate
CRF
Min Rate
Max Rate
Buffer size
VP8/VP9 Options
Video Filters
Aspect Ratio
FPS
Preset
Profile
Tune
Audio Codec
Audio Channels
Audio Rate
Audio bit-rate
Bit per Sample
Audio Normalization
Input Audio Map
Output Audio Map
Subtitles Map
Time Period RMS-based volume statistics Re-start is required Read and write useful notes and reminders. Ready References Refresh all log files Reload Remember that you can always change these settings later, through the Setup dialog. Remove Remove the selected files from the list Remove the selected preset from the Presets Manager Replace the selected preset with the Videomass default one Required: Microsoft Visual C++ 2010 Service Pack 1 Redistributable Package (x86) Reserved arguments for the first pass Reserved arguments for the second pass Reset Resizing Resizing filters Resolution Restore Restore all... Restore the default destination folders Restore the default folders for file conversions and downloads Restrict file names Result dBFS Retrieve all Videomass default presets Rotate 180 degrees Rotate 90 degrees left Rotate 90 degrees right Rotation Rotation setting Rows: Sample (aka Pixel) Aspect Ratio.
Set to 0 to disable Save all downloads to this temporary location Save each file in the same folder as input file Save the new profile to... Save.. Scale filter, set to 0 to disable Search for a specific frame Second pass of the selected profile Select a log file Select items to view them in text format Selected File
Output Format
Destination Folder
Rate (fps)
Resizing
Mosaic rows
Mosaic columns
Mosaic padding
Mosaic margin
Custom Arguments
Time Period Selected index does not exist or does not contain any audio streams Set FPS control from 0.1 to 30.0 fps. The higher this value, the more images will be extracted. Set a persistent location to save exported files Set a persistent location to save the file downloads Set how shaky the video is and how quick the camera is. A value of 1 means little shakiness, a value of 10 means strong shakiness. Default value is 5. Set integrated loudness target: Set loudness range target: Set maximal angle in radians (degree*PI/180) to rotate frames. Default value is -1, meaning no limit. Set maximum true peak: Set minimum contrast. Below this value a local measurement field is discarded. The range is 0-1. Default value is 0.25. Set optimal zooming to avoid borders. Accepted values are: `0` disabled, `1` optimal static zoom value is determined (only very strong movements will lead to visible borders) (default), `2` optimal adaptive zoom value is determined (no borders will be visible), see zoomspeed. Note that the value given at zoom is added to the one calculated here. Set percent to zoom maximally each frame (enabled when optzoom is set to 2). Range is from 0 to 5, default value is 0.25. Set percentage to zoom. A positive value will result in a zoom-in effect, a negative value in a zoom-out effect. Default value is 0 (no zoom). Set reference frame number for tripod mode. If enabled, the motion of the frames is compared to a reference frame in the filtered stream, identified by the specified number. The idea is to compensate all movements in a more-or-less static scene and keep the camera view absolutely still. The frames are counted starting from 1. Set stepsize of the search process. The region around minimum is scanned with 1 pixel resolution. Default value is 6. Set the accuracy of the detection process. A value of 1 means low accuracy, a value of 15 means high accuracy. Default value is 15. Set the camera path optimization algorithm. Values are: `gauss`: gaussian kernel low-pass filter on camera motion (default), and `avg`: averaging on transformations Set the interlacing filter coefficients. Set the number of frames (value*2 + 1) used for lowpass filtering the camera movements. Default value is 15. For example a number of 10 means that 21 frames are used (10 in the past and 10 in the future) to smoothen the motion in the video. A larger value leads to a smoother video, but limits the acceleration of the camera (pan/tilt movements). 0 is a special case where a static camera is simulated. Set the time interval between images in seconds (from 1 to 100 sec.), default is 1 second Set up a temporary folder for conversions Set up a temporary folder for downloads Setdar filter (display aspect ratio) example 16/9, 4/3  Setsar filter (sample aspect ratio) example 1/1 Setting timestamp Settings Shadow Color Sharpen or blur the input video. Note the use of the unsharp filter which is always recommended. Shortcut keys while playing with FFplay Show FFmpeg's built-in configuration capabilities Show Logs	Ctrl+L Show configuration Show text box Show the latest version... Show useful shortcut keys when playing or previewing with FFplay Showing log messages Shows available decoders for FFmpeg Shows available encoders for FFmpeg Shows download statistics and information Shows the latest version available on github.com Shows the text in the toolbar buttons Shows the version in use Simple interlacing filter from progressive contents. Size Some text boxes are still incomplete Sorry, all task failed ! Sorry, this preset is not part of default Videomass presets. Source size: {0} x {1} pixels Spaces around the mosaic borders. From 0 to 32 pixels Spaces around the mosaic tiles. From 0 to 32 pixels Specifies a maximum tolerance. This is only used in conjunction with buffer size Specifies a minimum tolerance to be used Specifies the decoder buffer size, which determines the variability of the output bitrate  Specify how to deal with borders that may be visible due to movement compensation. Values are: `keep` keep image information from previous frame (default), `black` fill the border black Specify type of interpolation Specify type of interpolation. Available values are: `no` no interpolation, `linear` linear only horizontal, `bilinear` linear in both directions (default), `bicubic` cubic in both directions (slow) Specify which frames to deinterlace. Stabilizer Statistics Statistics viewer Still Image Maker Still Image Maker	Shift+I Stops current process Subtitle Subtitle Streams Successful changes! Successful recovery Successful storing! Successfully completed ! Successfully downloaded to "{0}" Supported Format List Supported Formats list (optional). Do not include the `.` Suppress excess output System TITLE SELECTION Target level: Thank You! The URLs contain channels. Are you sure you want to continue? The URLs contain playlists. Are you sure you want to continue? The audio Rate (or sample-rate) is the sound sampling frequency and is measured in Hertz. The higher the frequency, the more true it will be to the sound source and the more the file will increase in size. For audio CD playback, set a sampling frequency of 44100 kHz. If you are not sure, set to "Auto" and source values will be copied. The audio bitrate affects the file compression and thus the quality of listening. The higher the value, the higher the quality. The chosen icon theme will only change the icons,
background and foreground of some text fields. The downloader is disabled. Check your preferences. The file is not a frame or a video file The files do not have the same "codec_types", same "sample_rate" or same "width" or "height". Unable to proceed. The following settings affect output messages and
the log messages during transcoding processes.
Change only if you know what you are doing.
 The preset "{0}" was successfully removed The preset was exported successfully The presets database has been successfully updated The process was stopped due to a fatal error. The search function allows you to find entries in the current topic The selected profile command has been changed manually.
Do you want to apply it during the conversion process? The selected profile is not suitable to convert the following file formats:

%s

 The timestamp size does not auto-adjust to the video size, you have to set the size here There are still processes running.. if you want to stop them, use the "Abort" button.

Do you want to kill application? There is a new version available {0} This feature allows you to download video and audio from
many sites, including YouTube.com and even Facebook. This filter is incompatible with 2-pass enabled This preset already exists and is about to be updated. Don't worry, it will keep all your saved profiles.

Do you want to continue? This will update the presets database. Don't worry, it will keep all your saved profiles.

Do you want to continue? Threads used for transcoding (from 0 to 32): Timestamp settings Title To exit click the "Finish" button Toolbar customization Tools Topic List... Translation... Transpose Filter Trash folder Tune Two-Pass Two-Pass (optional), Do not start with `ffmpeg -i filename`; do not end with `output-filename` URL URL list changed, please check the settings again. URLs have no playlist references Unable to get format codes on '{0}'

Unsupported '{0}':
'{1}' Unable to preview Video Stabilizer filter Undetected volume values! Click the "Volume detect" button to analyze audio volume data. Unexpected error while creating file:

{0} Unexpected error while deleting file contents:

{0} Unset Unsharp filter: Unsupported format '{}' Update the presets list Url Use a temporary location to save conversions Use for Web User Guide Version in Use Version {} Video Video Codec Video Detect Video Encoder Video Filters Video Stream Video stabilizer filter Video transform Video width and video height ratio. Videomass Videomass - AV Conversions Videomass - Concatenate Demuxer Videomass - Downloading... Videomass - From Movie to Pictures Videomass - Loading... Videomass - Output Monitor Videomass - Presets Manager Videomass - Queued Files Videomass - Queued URLs Videomass - Still Image Maker Videomass - YouTube Downloader Videomass Wizard Videomass already seems to include FFmpeg.

Do you want to use that? Videomass has a simple graphical
interface for youtube-dl and yt-dlp
 Videomass is an application based on FFmpeg
 Videomass supports mono and stereo audio channels. If you are not sure set to "Auto" and source values will be copied. Videomass: Please Confirm View Viewing last log Viewing log messages Volume Statistics Volume detect WARNING: The selected URL does not refer to a playlist. Only lines marked green can be indexed. Wait....
Audio peak analysis. Wait....
Retrieving required data. Wait....
The archive is being downloaded Warn on exit Warning Welcome to Videomass Welcome to the Videomass Wizard! When it's grey...

...It means the resulting audio will not change,
because it's equal to the source. When it's red...

...It means the resulting audio will be clipped,
because its volume is higher than the maximum 0 db
level. This results in data loss and the audio may
sound distorted. When it's yellow...

...It means an audio signal will be produced with
a lower volume than the original. When not available, the chosen video resolution will be replaced with the closest one Where do you prefer to save your downloads? Where do you prefer to save your transcodes? Which dimension to adjust? While playing Width Width: Wiki Wizard completed successfully!
 Work Notes	Ctrl+N Write subtitles to video You are using '{0}' version {1} You are using a development version that has not yet been released!
 YouTube Downloader YouTube downloader	Shift+Y [CROP] Specify how to deal with borders at movement compensation [INVERT] Invert transforms if checked [OPTALGO] optimization algorithm [RELATIVE] Consider transforms as relative to previous frame if checked, absolute if unchecked [TRIPOD2] virtual tripod mode, equivalent to relative=unchecked:smoothing=0 [TRIPOD] Enable tripod mode if checked [Videomass]: FAILED ! [Videomass]: SUCCESS ! cache folder has not been created yet. description flags format fps help topic list hqdn3d options hqdn3d:
This is a high precision/quality 3d denoise filter. It aims to reduce image noise, producing smooth images and making still images really still. It should enhance compressibility. list sinks of the output device list sources of the input device lowpass:
Enable (default) or disable the vertical lowpass filter to avoid twitter interlacing and reduce moire patterns.
Default is no setting. mode
The interlacing mode to adopt. nlmeans options nlmeans:
Denoise frames using Non-Local Means algorithm is capable of restoring video sequences, even with strong noise. It is ideal for enhancing the quality of old VHS tapes. options parity
The picture field parity assumed for the input interlaced video. please try again. print all options (very long) print basic options print more options q, ESC
f
p, SPC
m
9, 0
/, *
a
v
t
c
w
s

left/right
down/up
page down/page up

right mouse click
left mouse double-click scan:
determines whether the interlaced frame is taken from the even (tff - default) or odd (bff) lines of the progressive frame. show available HW acceleration methods show available audio sample formats show available bit stream filters show available color names show available devices show available filters show available pixel formats show available protocols specifies the target (average) bit rate for the encoder to use. Higher value = higher quality. Set -1 to disable this control. start  {} | duration  {} status wait... I'm interrupting wait... all operations will be stopped at the end of the download in progress. {0}: Latest version available: {1} {}

Sorry, removal failed, cannot continue.. {} file in queue Project-Id-Version: Videomass 4.0.5
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-01-18 00:31+0100
Last-Translator: José Alberto Valle Cid j.alberto.vc@gmail.com
Language-Team: Spanish <>
Language: es_MX
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
 

Las nuevas versiones corrigen fallos y añaden características. 

Videomass versión. {0}

 
...No encontrado 
  ..Nada disponible 
Primero, elija un tema en la lista desplegable  ¿Desea habilitar esta característica? "Auto" mantiene yodos los flujos de audio pero solo procesa el indice seleccionado; "Todos" mantiene y procesa todos los flujos de audio con las propiedades del indice seleccionado; "Solo el Indice" procesa y matiene solo el flujo de audio correspondiente al indice seleccionado. "Video" para guardar el archivo de salida como  video; "Audio" para guardar como solo audio "cpu-usada" fija que tan eficiente sera la compresión. El significado depende de los modos de arriba. Se requiere "ffmpeg", "ffprobe" y "ffplay". Complete todas
las cajas de texto presionando en los botones respectivos. "buena" es la predeterminada y recomendada para muchas aplicaciones; "mejor" recomendada si tiene mucho tiempo y quiere la mejor eficiencia en la compresión; "tiempo real" recomendada para directos/rápida codificación '¡Exito!

Se ha creado un nuevo preset vacio. '{}' no esta instalado en su equipo. Instalelo o indique su ubicación presionando el botón 'Localizar'. ...Completado ...Interrumpido 180° 90° (izq) 90° (der) < Anterior =  Pico inferior máximo =  Picos recortados =  Sin cambios Ya existe archivo con ese nombres, ¿quiere sobreecribirlo? Una descripción amplia del perfil Se importo exitosamente un nuevo preset Una nueva versión esta disponible . {0}
 Un conjunto de herramientas útiles para conversiones de audio y video. Guarde sus perfiles y reutilicelos con el Administrador de Presets. Un nombre corto de perfil Se debe seleccionar un archivo destino en la lista de archivos Una herramienta util para buscar temas de la ayuda de FFmpeg Conversiones A/V 	Shift+V Conversiones-AV Abortar Sobre Videomass Activar normalización RMS, se calcula la ganancia de acuerdo al volumen medio para lograr la misma potencia promedio de señal. Activa normalización de nivel de pico, produce un nivel de pico máximo igual al configurado. Activar la normalización de dos pasos. Se Normaliza la intensidad percibida utilizando el filtro "​loudnorm", el cual implementa el algoritmo EBU R128. Activa row-mt 1 Añadir Añadir Archivos Añadir perfil Añadir nuevo perfil desde este panel con la configuración actual Añadir metadatos al archivo Argumentos Adicionales Opciones Avanzadas Se han recuperado todos los presets con éxito Todos los presets exportados con exito Ya existe: 

{}

¿Quiere reescribirlo?  Se debe seleccionar en la lista de URLs Apariencia Preferencias de la aplicación Aplicar Aplica Filtros ReduceRuido ¿Seguro que desea vaciar la papelera? ¿Seguro desea limpiar el archivo de registro? ¿Seguro que desea borrar el perfil seleccionado? No sera posible recuperarlo. ¿Seguro que desea salir? ¿Seguro que desea borrar el preset "{}" ?

 It will be moved to the "Removals" subfolder of the presets folder. Relación de Aspecto Asignar un sufijo opcional a los archivos de salida: Al menos un "Código de Formato" debe seleccionarse en las URL en verde. Se requieren al menos dos archivos para realizar la encadenación. Parte inferior de la ventana A la izquierda de la ventana A la derecha de la ventana Parte superior de la ventana (predeterminado) Audio Codec de Audio Codificador de Audio Filtros de Audio Propiedades de Audio Flujos de Audio Mapeo de Flujos de Audio La normalización de Audio solo es requerida por algunos archivos Normalización de audio apagada El proceso de normalización de Audio no puede guardarse en el Administrador de Presets. No se aplicara la normalización de Audio, la señal de la fuente es igual Crear sub carpetas al descargar listas de reproducción Auto-detectar Salir al terminar reproducción Codecs de video disponibles. "Copiar" no es un codec, pero indica que el flujo de video no sera recodificado y permite cambiar el formato u otros parámetros Regresar ¡Sea cuidadoso! Se sobreescribira el preset seleccionado con el predeterminado. ¡Todos sus perfiles serán eliminados!

¿Desea continuar? ¡Sea cuidadoso! Se sobreescribira todos los preset con los predeterminados. ¡Todos sus perfiles serán eliminados!

¿Desea continuar? La profundidad de bites el numero de bits de información en cada muestra, y se corresponde directamente a al resolución de cada muestra. La profundidad de bit solo es relevante para señales digitales PCM. Los formatos No-PCM, como los formatos compresos/con perdidas, no tienen asociados una profundidad de bits. Color de la Caja Asignar un sufijo adicional evita la reescritura de archivos CAPACIDADES DE CODFICACIÓN Carpeta Cache No se puede continuar: La duración de la linea de tiempo excede la de algunos archivos. No se puede guardar la información actual en '{}'. Centro Cambiar el tamaño y color de la estampa de tiempo durante la reproducción Los cambios tendrán efecto al reiniciar el programa.

¿Quiere salir ahora? Comprobar nuevos presets Comprobar nueva version Busque la versión mas reciente de Videomass en <https://pypi.org/project/videomass/> Comprobar nuevas versiones de presets de Videomass en su pagina Revise la salida actual o el archivo de registro para más información. Comprobando versión más reciente Elija una carpeta de descarga Elija una carpeta para exportar todos los presets Elija una carpeta para guardar el preset seleccionado Elija un destino para la papelera Elija un preset y vea sus perfiles Elija un destino temporal para las conversiones Elija un destino temporal para las descargas Elija un tema de la lista Seleccione un indice de los flujos de  audio disponibles. Si el archivo fuente es un video, se recomienda seleccionar un indice numérico. Si el archivo fuente es un audio, deje este  control en "Auto". Elija el  ejecutable de {} Quitar todos los filtros habilitados  Limpiar archivo de registro seleccionado Limpiar cache al salir de la aplicación Presione en la columna "Contenido de la Lista" para especificar indices de los videos separados por comas en la lista de reproducción ej: "1,2,5,8" si desea descargar los videos con los indices 1, 2, 5, 8 de la lista.

Puede especificar rangos: "1-3,7,10-13" descargara los videos con los indices 1, 2, 3, 7, 10, 11, 12 y 13.
 Presione el botón "Siguiente" Presione el botón "Siguiente" para iniciar Cerrar Videomass Columnas: Encadenar Dezmezclador	Shift+D Encadenar archivos de medios Encadenar múltiples archivos de medios en base a su orden de importación sin recodificar Configuración de carpeta ¡Felicitaciones! Ya esta utilizando la ultima versión versión.
 Marcado considera las transformaciones como relativas al cuadro previo, desmarcado como absolutas. Desmarcado es lo predeterminado. Factor de frecuencia Constante. Valores mas bajos = mejor calidad y mayor tamaño de archivo.  -1 deshabilita este control. Restringir proporciones (mantener relación de aspecto) Contenedor: Carpeta de conversiones	Ctrl+C Convertir Convertir usando FFmpeg Crear nuevo preset Crear nuevo perfil Crear un nuevo perfil y guardarlo en el preset seleccionado Crear nuevo perfil en el preset "{}" Crear GIF animado Crear miniaturas Crear mosaicos Crear video a partir de una secuencia de imágenes, ordenadas según se importan, con la posibilidad de añadir un archivo de audio file. Crear, editar y usar rápidamente sus presets y perfiles favoritos de FFmpeg con un completo soporte de formatos y codecs. Filtro de Recorte Recortar altura Recortar ancho Recortar Recortar area seleccionada  Interfaz grafica multiplataforma para FFmpeg y youtube-dl.
 Lugar actual: CAPACIDADES DE DECODIFICACIÓN DESHABILITADO: Formato de Datos Limite/Calidad Decodificadores Carpetas destino predeterminadas restablecidas con éxito Posición predeterminada Desentrelazado Desentrelazar el video de entrada con el filtro `w3fdif` Desentrelazar el video de entrada con el filtro `yadif` . Utilizar FFmpeg, es la mejor opción y la más rápida Desentrelazar con el filtro w3fdif Desentrelazar con el filtro yadif Desentrelazado Desenetrelazar y entrelazar Borrar Borrar todo de la lista Borrar todo de la Papelera de Videomass Borrar todo el texto de la lista Borrar archivos de registo al salir de la aplicación Borrar perfil seleccionado Solo Desmezclar Soporte Desmezclar/Mezclar ReduceRuido Filtros ReduceRuido Denominador Descripción Carpeta destino No se permiten Carpetas, por favor solo añada archivos. Deshabilitado Relación de Aspecto de Pantalla. Poner a 0 para deshabilitar Mostrar objetos seleccionados en formato texto Mostrar estampas de tiempo Mostrar estampas de tiempo al reproducir películas de FFplay No mostrar nuevamente este dialogo Donaciones ¡Terminado! No comprobar certificado SSL Descargar Descargar solo Audio Descargar todos los presets de Videomass desde su pagina Descargar todos los subtitulos disponibles Descargar todos los videos en la lista de reproducción Descargar por código de formato Descargar archivos usando youtube-dl Descargar por separado audio y video Descargar solo subtitulos Descargar los presets más recientes Descargar videos según su resolución Gestor Preferencias de Gestor de Descargas Carpeta de Descargas	Ctrl+D Arrastre uno o más archivos abajo Duplicado Se desechan archivos duplicados: > {} Duración Duración: HABILITADO: ERROR ERROR: URL invalida: "{}" ERROR: Opción invalida ERROR: Falta flujo de audio:
"{}" ERROR: Se encontraron URLs repetidas ERROR: Error de escritura en llaves JSON: {}

Archivo: "{}"
¿Llave mal formada ? ERROR: No se puede obtener códigos de formato.

URL no soportada:
'{0}' ERROR: {0}

{1} no esta instalado, use su administrador de paquetes para hacerlo. Descargue fácilmente video y audio en diferentes formatos y calidades desde YouTube, Facebook y otros sitios. Editar Editar perfil del preset "{}" Editar perfil seleccionado Empotrar miniatura en archivo de audio Vaciar Papelera Habilitar solo una imagen fija Habilitar otra localización para FFmpeg Habilitar otra localización para FFplay Habilitar otra localización para FFprobe Habilitar filtro hqdn3d Habilita filtro nlmeans Habilitar estabilizador Habilitar formato texto Habilita el modo de tripie virtual, equivalente a relative=0:smoothing=0. Desmarcado por omisión. Use junto a la opción tripod de vidstabdetect. Habilitado Codificadores Añada URLs a continuación Intorduzca nombre del nuevo preset Teclee solo caracteres alfanuméricos. Puede utilizar el guion ("-") y el guion bajo ("_"). No se permiten espacios. Error, preset invalido: "{}" Salir Salir	Ctrl+Q Exportar todo... Exportar toda la carpeta de presets como copia Exportar seleccionado Exportar preset seleccionado como copia Extensión Extraer cuadros desde películas  a imágenes  en formatos JPG, PNG, BMP, GIF . FFmpeg Configuración de FFmpeg Decodificadores FFmpeg Documentación de FFmpeg Codificadores FFmpeg Formatos de archivos FFmpeg Temas de ayuda de FFmpeg Nivel de registro de FFmpeg FFplay ¡No se encontró FFplay ! ¡No se encontró FFprobe ! SELECCIÓN DE ARCHIVO ¡Error Fatal ! Archivo Nombre de Archivo Preferencias de Archivo Archivo no existe o invalido:  %s Archivo no existe:

"{}"
 Cambio en lista de archivos, revise las configuraciones de nuevo. Nombre de archivo Archivo a encadenar
Archivo de Salda
Destino
Formato de Salida
Periodo de Tiempo Archivo a procesar
Nombre de archivo de salida
Carpeta Destino
Formato de Salida
Argumentos Adicionales
Archivo de Audio
Más corto
Redimensionar
Pre-input
Cuadros Por Segundo (FPS)
Duración de la imagen fija
Duración general del video Archivo sin extensión de formato: ponga una extensión apropiada al archivo, ejemplo '.mkv', '.avi','.mp3', etc. Finalizar Primer paso del perfil seleccionado Primero elija un perfil en la lista Primero, elija un tema en la lista desplegable Deshabilitar banderas Habilitar banderas Configurar banderas Color de Letra Tamaño de Letra Para más detalles, vea la Guia de Usuario de Videomass: Para más información, visite la documentación oficial de FFmpeg: Forzar la relación de aspecto original usando
rellenar en lugar de estirar Código de Formato Debe separar con coma los formatos Los cuadros se repiten un dado numero de veces por segundo. En algunos países es 30 para NTSC, en otros  (como Italia) usan 25 para PAL De Película a Imágenes De Película a Imágenes	Shift+S De una secuencia de imágenes a un archivo de video Obtiene información de flujos Multimedia Generar video dual para comparaciones Obtenga la Ultima Version Guarde sus archivos en el destino especificado Obtiene el volumen máximo y promedio en dBFs, y calcula el desplazamiento para normalizar el audio. Ir al panel de Conversiones de Audio/Video Ir al panel 'Encadenar Desmezcladores' Ir al panel 'De Película a Imágenes' Ir al panel Principal Ir al panel del Administrador de presets Ir al panel 'Productor de Imágenes Fijas' Ir al panel 'YouTube Downloader' Ir al proximo panel Ir al panel anterior Ir a Altura Alto: Ayuda Visualizador  de ayuda Panel principal	Shift+H Como instalar pip en su distribución Linux Como actualizar un paquete de Python Tamaño de icono: Temas de iconos Si FFmpeg no esta en su equipo, esta aplicación es inútil Si se activa, algunos mensajes de salida se ocultan. Marcado, la ventana de FFplay se cerrara al finalizar la reproducción Si ya ha instalado FFmpeg en su sistema
operativo, presione el botón "Auto-detectar". Si desea mantener la relación de aspecto, seleccione "Restringir proporciones"  y
especifique sólo una dimensión, ya sea la anchura o la altura, y fije la otra dimensión
a -1 o -2 (algunos códecs requieren -2, por lo que deberá hacer algunas pruebas primero). Si quiere usar una versión de FFmpeg localizada en
el sistema de archivos pero no instalada en el sistema operativo,
presione el botón "Localizar". Ignorar mayúsculas Ignorar mayúsculas: las letras mayúsculas y minúsculas serán iguales. Las Imágenes deben redimencionarse, por favor use la función Redimensionar. Importar un grupo de presets desde una carpeta y actualizar los existentes Importar nuevo preset Importar nuevo preset o actualizar uno existente Importar nueva carpeta de presets Importar grupo Importar preset Incluir archivo de audio Incluir el ID de video
en los nombres de archivo Asignaciones de perfil incompletas Selecciòn de Indice: Informaciones Integrated Loudness Target in LUFS. From -70.0 to -5.0, default is -24.0 Entrelazado Entrelazar con el filtro entrelazar ¡Proceso Interrumpido ! Archivo invalido: "{}" Preset invalido.
Se recomienda eliminarlo o reescribirlo en un formato JSON compatible con Videomass.

Posible solución: abra el panel del Administrador de Presets, ir a la columna presets y presionar el botón "Restaurar" Si se marca Invierte transformaciones. Desmarcado es lo predeterminado. ¿Esta 'ffmpeg' instalado en su sistema? Seguimiento de fallos Puede reducir el tamaño del archivo, pero dilatar más. Mantener rastro de la salida para depuración de errores Nivel Limita por máximo nivel de pico o por nivel promedio (cuando se usa RMS) en dBFS. Desde -99 a +0.0; predeterminado para nivel de pico es -1.0; para RMS es 20.0 Escuchar Cargar Cargar cuadro Localizar Localizando ejecutables de FFmpeg
 Lista de archivos de registro Carpeta de registros Registro de mensajes Rango de Intensidad Destino en LUFS. Desde +1.0 a +20.0, predeterminado +7.0 Asegúrese de utilizar la ultima versión de
'{}'. Esto le evita problemas con las descargas.
 Mapear: Volumen máximo dBFS Verdadero Pico Máximo en dBTP. Desde -9.0 a +0.0, predeterminado -2.0 Volumen promedio dBFS Flujos Multimedia Tipo de Medio Medio: Miscelánea Mover horizontalmente - ponga -1 para centrar en el eje horizontal Después de conversión exitosa mover archivo fuente a la papelera de Videomass Mover verticalmente - ponga -1 para centrar en el eje vertical Analizador de flujos Multimedia Mezcladores y Dezmezcladores Mezcladores y Dezmezcladores disponibles en el FFmpeg ytilizado. Solo Mezclar Nombre Nuevo Nuevo tamaño en pixeles Siguiente Siguiente > Sin Audio No hay archivos a borrar No hay nueva versión disponible No se encontraron presets.

Posible solución: abra el panel del Administrador de Presets, ir a la columna presets y presionar el botón "Restaurar todo..." No existe carpeta '{}' Normalización No instalado No todo fue exitoso. No encontrado Numerador Indices de video a descargar de la Lista Apagado Desplazamiento dBFS Uno o más nombres de formato separados con coma a incluir en el perfil Un-Paso Un-Paso, No inicie con `ffmpeg -i archivo`; no termine con `archivo-destino` Solo se puede reproducir un video a la vez.

No soportado '{0}':
'{1}' Abrir un archivo de audio Abrir archivo de audio Abra uno o más archivos Abrir conversiones temporales Abrir Descargas temporales Si existe, abre la Papelera de Videomass Abrir la carpeta de Descargas predeterminada Abrir la carpeta de conversiones predeterminada Abrir archivo seleccionado Abrir carpeta de Descargas temporales Abrir carpeta de conversiones temporales Abrir...	Ctrl+O Si existe, abre la carpeta cache de Videomass Abrir la carpeta de configuración de Videomass Si existe, abre la carpeta de registros de Videomass Optimizaciones Opciones Otras opciones Otros recursos no oficiales: Formato de Salida Formato de Salida. Vacio, copia el formato y codec. No incluya el `.` Monitor de Salida	Shift+O Carpeta destino no existente:

"{}"
 Extensión del formato de salida. Deje vació para copiar codec y formato Formato de Salida: Estadísticas de volumen basadas en Picos Parámetros Trayectoria de los ejecutables Permiso negado: {}

Compruebe permisos de ejecución. Situar la barra de herramientas Reproducir Reproducir y escuchar el resultado de los filtros de audio Reproducir url seleccionada Reproducir archivo seleccionado en la lista Reproducir video hasta que la pista de audio finalice Editor de Lista de Reproducción Contenido de la Lista Indices de video a descargar de la Lista Por favor Confirme Por favor Confirme Por favor configure la aplicación Referencias post-normalización: Videos precompilados Preferencias	Ctrl+P Formato de video preferido Preset Presets Administrador de Presets Administrador de presets	Shift+P Evitar reescritura de archivos Previsualizar Previsualizar filtros de video Registro del proceso: Procesando... Perfil Nombre de Perfil Ya existe perfil con ese nombre Perfiles Calidad Calidad/Velocidad
modificar relación: Archivo Añadido
Formato de Salida
Optimizar para Web
Codec de Audio
Frec. bits de Audio
Canales de Audio
Taza de Audio
Bit por Muestra
Normalización de Audio
Periodo de Tiempo
Mapa del Audio de Entrada Archivo Añadido
Paso de Codificación
Perfil Usado
Formato de Salida
Periodo de Tiempo Archivo Añadido
Optimizar para Web
Formato de Salida
Codec de Video
Razón de Aspecto
CPS
Codec de Audio
Canales de Audio
Taza de Audio
Frec. bits de Audio
Bit por Muestra
Normalización de Audio
Mapa del Audio de Entrada
Mapa del Audio de Salida
Mapa de Subtítulos
Periodo de Tiempo Archivo Añadido
Optimizar para Web
Paso de Codificación
Formato de Salida
Codec de Video
Frec. de bits del Video
CRF
Frec. Min
Frec. Max
Tamaño del Buffer
Opciones VP8/VP9
Filtros de Video
Razón de Aspecto\n
CPS
Preset
Perfil
Ajuste
Codec de Audio
Canales de Audio
Taza de Audio
Frec. bits de Audio
Bit por Muestra
Normalización de Audio
Mapa del Audio de Entrada
Mapa del Audio de Salida
Mapa de Subtítulos
Periodo de Tiempo Estadísticas de volumen basadas en RMS Reinicie la aplicación Lea y escriba recordatorios y notas. Listo Referencias Refrescar todos los registros Recargar Recuerde que puede cambiar estas configuraciones posteriormente, mediante el dialogo Configurar. Eliminar Eliminar archivos seleccionados de la lista Eliminar preset seleccionado del Administrador de Presets Reemplace preset seleccionado con el predeterminado de Videomass Requerido: Microsoft Visual C++ 2010 Redistributable Package (x86) Argumentos reservado para el primer paso Argumentos reservado para el segundo paso Reiniciar Redimencionar Filtros de Redimensionado Resolución Restablecer Restablecer todo... Restablecer carpetas destino predeterminadas Restablecer carpetas predeterminadas para conversiones y descargas Restringir los nombres de archivo Resultado dBFS Recuperar todos los presets predeterminados de Videomass Rotar 180 grados Rotar 90 grados a la izquierda Rotar 90 grados a la derecha Rotación Rotación Renglones: Relación de Aspecto de Muestra (Pixel) .
Poner a 0 para deshabilitar Guardar todas las descargas en esta carpeta temporal Guardar en la carpeta del archivo de entrada Guardar nuevo perfil en... Guardar... Filtro Escala, poner a 0 para deshabilitar Buscar un cuadro especifico Segundo paso del perfil seleccionado Seleccionar archivo de registro Seleccionar objetos a ver en formato texto Archivo Seleccionado
Formato de Salida
Carpeta Destino
Velocidad (fps)
Redimensionar
Renglones de Mosaicos
Columnas de Mosaicos
Rellenado de Mosaicos
Margen de Mosaicos
Argumentos Personales
Periodo de Tiempo El indice seleccionado no existe o no contiene flujos de audio Fijar el control de FPS, desde 0.1 hasta 30.0 fps. Entre más alto el valor se extraen más imágenes. Elija una localización permanente para guardar los archivos exportados Elija una localización permanente para guardar las descargas Fija el grado de temblor del vídeo y la rapidez de la cámara. Un valor de 1 significa poca vibración, un valor de 10 significa mucha vibración. El valor predeterminado es 5. Fijar intensidad de sonido objetivo: Fijar rango objetivo de intensidad de sonido: Fija angulo máximo en radianes (grados*PI/180) para rotar los cuadros. Valor predeterminado -1, significa sin limites. Fijar pico máximo verdadero: Fija el contraste mínimo. Bajo este valor la medida del campo local es descartada. El rango es 0-1. Valor predeterminado 0.25. Fija el aumento optimo para evitar bordes. Valores aceptados: `0` desactivado, `1` se determina el nivel de aumento optimo (solo los movimientos muy fuertes producen bordes visibles) (predeterminado), `2` se determina el nivel de aumento optimo adaptativo (no habrá bordes visibles), vea zoomspeed. Note que el valor dado en aumento se añade al calculado aquí. Fija el porcentaje de aumento máximo en cada cuadro (habilitado cuando optzoom se fija a 2). Rango de 0 to 5, valor predeterminado 0.25. Fija el porcentaje de aumento. Un valor positivo produce efecto de acercamiento, un valor negativo de alejamiento. Valor predeterminado 0 (sin aumento). Fija el numero de cuadro de referencia para el modo tripod. Si se habilita, el movimiento de los cuadros se compara con un cuadro de referencia en el flujo filtrado, identificado por el numero especificado. The idea is to compensate all movements in a more-or-less static scene and keep the camera view absolutely still. The frames are counted starting from 1. Fija el paso del proceso de búsqueda. La resolución mínima de la región es de 1 pixel. Valor predeterminado 6. Fija la precisión del proceso de detección. Un valor de 1 significa poca precisión, un valor de 15(predeterminado) significa alta precisión. Fija el algoritmo de optimización para la trayectoria de camera. Valores: `gauss`: filtro pasabajos núcleo gaussiano para el movimiento de cámara (predeterminado), y `avg`: promedio de transformaciones Fijar los coeficientes del filtro de entrelazado. Fijar el numero de cuadros (valor*2 + 1) usados por pasabajos para filtrar movimientos de cámara. Valor predeterminado 15. Por ejemplo un valor de 10 significa que  se utilizan 21 cuadros (10 en el pasado 10 en el futuro) para suavizar el movimiento en el video. Un valor mayor produce un vídeo más suave, pero limita la aceleración de la cámara (movimientos de giro/inclinación). 0 es un caso especial en el que se simula una cámara estática. Fijar intervalo en segundos entre imágenes (from 1 to 100 sec.),  1 es lo predeterminado Fijar carpeta temporal para conversiones Fijar carpeta temporal para descargas Filtro Setdar  (relación de aspecto de pantalla) ejemplo 16/9, 4/3  Filtro Setsar  (relación de aspecto de la muestra) ejemplo 1/1 Configurar estampa de tiempo Configuraciones Color de Sombra Enfoca o desenfoca el vídeo de entrada.. Se recomienda siempre usar el filtro de desenfoque. Atajos del teclado al reproducir con FFplay Mostrar capacidades de  configuración integradas en FFmpeg Mostrar Registros	Ctrl+L Mostrar configuración Mostrar caja de texto Mostrar ultima versión. Mostrar atajos de teclado al reproducir o previsualización con FFplay Mostrando registro de mensajes Muestra los decodificadores disponible en FFmpeg Muestra los codificadores disponible en FFmpeg Mostrar estadísticas de descargas e información Mostrar ultima versión disponible en github.com Mostrar texto en los botones de la barra de herramientas Mostrar versión en uso Filtro de entrelazado simple proveniente de contenidos progresivos. Tamaño Algunas cajas de texto están incompletas ¡Ha fallado la tarea ! Este preset no es parte de los presets predeterminados de Videomass. Tamaño de la fuente: {0} x {1} pixeles Espacio entre bordes de mosaicos. Desde 0 hasta 32 pixels Espacio entre mosaicos. Desde 0 hasta 32 pixels Especificar tolerancia maxima. Solo debe usarse en conjunto con tamaño de buffer Especificar tolerancia mínima a utilizar Especificar el tamaño de buffer del decodificador, determina la variabilidad de la frecuencia de bits de salida  Especifica como lidiar con los bordes visibles al compensar el movimiento. Valores: `mantener` mantener la información de la imagen del cuadro previo (predeterminado), `negro` rellenar lo negro en el borde Especificar tipo de interpolación Especifica tipo de interpolación. Valores disponibles: `no` = sin interpolación, `linear` lineal solo horizontal, `bilinear` lineal en ambas direcciones (predeterminado), `bicubic’`cubica en ambas direcciones (lenta) Especificar los cuadros a desentrelazar. Estabilizador Estadísticas Visualizado de Estadisticas Productor de Imágenes Fijas Productor de Imágenes Fijas	Shift+I Detener proceso actual Subtitulo Flujos de Subtítulos ¡Cambios exitosos! Recuperación exitosa ¡Guardado con exito! ¡Completado exitosamente ! Descargado exitosamente en "{0}" Lista de formatos soportados Lista de formatos soportados (opcional). No incluya el `.` Elimina exceso de salida Sistema SELECCIÓN DE TITULO Nivel Objetivo: ¡Gracias! Las URLs contienen canales. ¿Seguro que desea continuar? Las URLs contienen listas de reproducción. ¿Seguro que desea continuar? La Frecuencia de audio (o frecuencia de muestreo) es la frecuencia de toma de muestras del audio y se mide en Hertz. A mayor frecuencia, la fuente de sonido tendrá mayor fidelidad y se incrementara el tamaño del archivo. Para calidad CD , elija una frecuencia de muestreo de 44100 kHz. Si no esta seguro elija "Auto" y se copiaran los valores de la fuente. La frecuencia de bits (bitrate) del audio afecta la compresión y la calidad de audio. Los valores mas altos implican mejor calidad. El tema de iconos solo cambia los iconos,
fondo y primer plano de algunos campos de texto. Gestor de descargas esta desactivado. Revise sus configuraciones. El archivo no es un cuadro o archivo de video Los archivos no tienen los mismos "codec", "frec. muestreo" o "ancho" o "altura". No se puede proceder. Las siguientes configuraciones afectan los mensajes de salida
y del registro durante los procesos de transcodificación.
Cámbielas solo si sabe lo que esta haciendo.
 El preset "{0}" se elimino con exito El preset "se elimino con exito Base de datos de presets actualizada con exito Proceso detenido debido a error fatal. La función de búsqueda le permite encontrar entradas del tema actual El comando del perfil seleccionado se cambio manualmente.
¿Desea aplicarlo durante el proceso de conversión? El perfil seleccionado no es adecuado para convertir los siguiente formatos de archivo:

%s

 El tamaño de la estampa de tiempo no se auto ajusta al tamaño de video, configure aquí el tamaño Aun hay procesos ejecutandose. si desea pararlos, use el bnotón "Abortar" .

¿Desea terminar la aplicación? Disponible una nueva versión {0} Esta característica le permite descargar formatos de video y audio desde
muchos sitios, incluyendo YouTube.com e incluso Facebook. Este filtro no se puede usar si esta activado 2-pasos Este preset ya existe y sera actualizado. No se preocupe, se mantendrán sus perfiles guardados.

¿Desea continuar? Esto actualizara la base de datos de presets. No se preocupe, se conservaran todos sus perfiles guardados.

¿Desea continuar? Hilos usados para transcodificar(de 0 a 32): Configurar Estampa de tiempo Titulo Para salir presione el botón "Finalizar" Personalizar barra de Herramientas Herramientas Lista de Temas... Traducción... Filtro de transposición Carpeta de la Papelera Ajustar Dos-Pasos Dos-Pasos (opcional), No inicie con `ffmpeg -i archivo`; no termine con `archivo-destino` URL Cambio en lista de URLs, revise las configuraciones de nuevo. La URL no es una lista de reproducción No se puede obtener códigos de formato para '{0}'

'{0}'  no soportado:
'{1}' No se puede previsualizar el filtro Estabilizador de Video ¡No se detectaron valores de volumen! Presione el botón "Detectar Volumen" para analizar los datos del audio. Error inesperado al crear archivo:

{0} Error inesperado al borrar contenido de archivo:

{0} No fijado Filtro desenfoque: Formato no soportado '{}' Actualizar lista de presets Url Utiliza una carpeta temporal para guardar conversiones Para uso en Web Guía de usuario Versión en uso Versión {} Video Codec de Video Detectar Video Codificador de Video Filtros de Video Flujo de Video Filtro estabilizador de video Transformación de video Proporción ancho y alto de video. Videomass Videomass - Conversiones AV Videomass - Encadenar Dezmezcladores Videomass - Descargando... Videomass -De Película a Imágenes Videomass - Cargando... Videomass - Monitor de Salida Videomass - Administrador de Presets Videomass - Archivos Pendientes Videomass - URLs Pendientes Videomass -Productor de Imágenes Fijas Videomass - YouTube Downloader Asistente de Videomass Videomass ya incluye FFmpeg.

¿Desea utilizarlo? Videomass tiene una interfaz
sencilla para youtube-dl
 Videomass es una aplicación basada en FFmpeg
 Videomass soporta canales de audio mono y estéreo. Si no esta seguro elija "Auto" y se copiaran los valores de la fuente. Videomass: Por favor Confirme Ver Ver ultimo mensaje de registro Ver mensajes de registro Estadísticas de Volumen Detectar Volumen ADVERTENCIA: La URL no es una lista de reproducción. Solo las lineas marcadas en verde pueden indexarse. Espere....
Analizando Picos de Audio. Espere....
Obteniendo información requerida. Espere....
El archivo esta siendo descargado Advertir al salir Advertencia Bienvenido a Videomass ¡Bienvenido al asistente de Videomass! ...Significa que el audio resultante no tendrá
cambios y sera igual al de la fuente. ..Significa que el audio resultante sera recortado,
ya que el volumen es más alto que el nivel máximo 
de 0 db. Esto provoca perdida de datos y el audio 
podría sonar distorsionado. ...Significa que la señal de audio se producirá
con un volumen menor al original. Cuando no este disponible, la resolución elegida se reemplazara con la más cercana ¿Donde prefiere guardar sus descargas? ¿Donde prefiere guardar sus resultados? ¿Que dimensión se ajustara? Mientras se reproduce Ancho Ancho: Wiki ¡Asistente completado con éxito!
 Notas de trabajo	Ctrl+N Escribir subtítulos al video Esta utilizando '{0}' versión {1} ¡Esta utilizando una versión de desarrollo!
 YouTube Downloader YouTube downloader	Shift+Y [CROP] Especifica como lidiar con los bordes al compensar el movimiento [INVERT] Al marcar, invierte las transformaciones [OPTALGO] algoritmo de optimización [RELATIVE] Marcado considera las transformaciones como relativas al cuadro previo, desmarcado como absolutas [TRIPOD2] modo de tripie virtual, equivalente a relative=desmarcado:smoothing=0 [TRIPOD] Al marcar se habilita el modo tripie [Videomass]: ¡FALLO ! [Videomass]: ¡EXITO ! carpeta cache no ha sido creada. descripción banderas formato cps lista de temas de ayuda opciones de hqdn3d hqdn3d:
Estes es un filtro reductor de alta precisión/calidad 3d . Su objetivo es reducir el ruido de la imagen, produciendo imágenes suaves y hacer que las imágenes fijas sean realmente fijas. Debe mejorar la compresibilidad. lista receptores del dispositivo de salida listar fuentes del dispositivo de entrada pasabajos:
Habilita (predeterminado) o deshabilita el filtro vertical pasabajos para evitar el entrelazado twitter y reducir  el efecto Moiré.
No esta configurado valor predeterminado. modo
El modo de entrelazado. opciones de nlmeans nlmeans:
Mejora los cuadros usando el algoritmo  Medios No-Locales, es capaz de restaurar incluso secuencias de video con fuerte ruido. Es ideal para mejorar la calidad de las antiguas cintas VHS. opciones paridad
La paridad del campo imagen asumida por el video entrelazado de entrada. favor de tratar de nuevo. imprimir todas las opciones (muy largo) imprimir opciones básicas imprimir más opciones q, ESC
f
p, SPC
m
9, 0
/, *
a
v
t
c
w
s

izquierda/derecha
abajo/arriba
pagina abajo/pagina arriba

click botón derecho
doble click botón izquierdo scan:
determina si el cuadro entrelazado proviene de las lineas pares (tff - predeterminado) o impares (bff) del cuadro progresivo. mostrar métodos de aceleración por HW mostrar formatos de muestreo de audio disponibles mostrar filtros disponibles para flujo de bits mostrar nombres de color disponibles mostrar dispositivos disponibles mostrar filtros disponibles mostrar formatos de pixel disponibles mostrar protocolos disponibles especifica la frecuencia de bits (promedio) que usara el codificador. Un valor más alto =  mejor calidad. -1 deshabilita este control. inicio  {} | duración  {} estado espere.. Abortando espere.. se detendrán todas las operaciones al finalizar la descarga en progreso. La ultima versión de {0} esta disponible: {1} {}

Fallo al eliminar, no se puede continuar.. {} archivo en espera 