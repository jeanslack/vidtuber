��    �     �  �  �/      `?  /   a?     �?     �?     �?  2  �?     B  G  D  ,   PE  $   }E  �   �E  Q   �F  ]   �F  w   IG  �   �G  :   �H  2   �H  p   �H     gI     sI     �I     �I     �I  
   �I     �I     �I     �I  B   �I  !    J  &   BJ  #   iJ  n   �J     �J  2   K  :   DK     K     �K     �K     �K  �   �K  i   ?L  �   �L     :M     MM  	   QM     [M  ;   gM     �M     �M     �M  4   �M  +   N  /   ?N  .   oN  
   �N     �N     �N     �N     �N  #   �N  5   O  b   NO     �O  r   �O     CP  ,   PP  J   }P  9   �P     Q     Q     0Q     GQ     fQ     lQ     xQ     �Q     �Q     �Q     �Q  3   �Q     �Q  E   R  J   [R  1   �R     �R     �R  �    S     �S     �S  �   �S  y   9T    �T  	   �U  C   �U     V     *V  X   7V  &   �V     �V  :   �V  g   �V     aW     wW  O   �W  5   �W  K   X     aX     |X  %   �X  +   �X  4   �X  %   Y  .   BY  ,   qY     �Y  �   �Y     �Z     �Z     �Z  ,   �Z  #  �Z     \  &   0\     W\     g\     p\     �\  J   �\     �\     ]  :   ]  h   P]  k   �]  )   %^  
   O^     Z^     t^     |^     �^     �^  7   �^  #   �^     _     *_     <_  e   Q_  l   �_     $`     0`     ?`     M`     V`  >   o`     �`     �`  	   �`     �`     �`     �`  1    a     2a     Ca  0   Oa  b   �a     �a     b     "b     0b     Nb     Ub  .   tb     �b  A   �b     c     c     -c     Ec     Nc     _c     kc     wc  4   �c     �c  )   �c  (   �c     d  2   .d     ad     ~d     �d     �d     �d     �d  8   �d      e     "e     Be     Ze     ze     �e     �e     �e  
   �e     �e     f     %f  	   Bf  "   Lf     of  	   xf     �f     �f     �f     �f  !   �f     �f  @   �f  :   @g  I   {g  h   �g     .h     3h     Sh     mh     �h     �h  %   �h  %   �h  &   �h     &i     =i     Ui     gi  �   zi     j     j  (    j     Ij     Nj     _j  w   yj     �j     k     k     k  $   ,k     Qk     ak  	   �k  G   �k     �k     �k     �k     �k     l     #l     7l     Jl     `l     gl     l     �l     �l     �l  	   �l     �l  &   �l     �l  !   m  3   :m  	   nm     xm  I   �m  �   �m  z   �n     o  "   o  "   8o  +   [o     �o     �o     �o  
   �o  	   �o  /   �o  >   �o  @   7p     xp     �p  �   �p     )q     @q  &   _q  )   �q  "   �q     �q  H   �q  /   /r  o   _r  !   �r  %   �r  (   s     @s  !   Ws  #   ys  $   �s     �s     �s     �s     �s     �s     t     	t     t  -   (t     Vt  
   vt     �t  D   �t  )   �t  D   �t  a   Au  �   �u  �   �v     w  D   "w  8   gw  @   �w     �w  -   �w     #x     ?x     Lx     Zx  &   mx     �x     �x     �x  H   �x  	   y      $y     Ey     [y  3   ny  �   �y  3   �z  %   �z     �z  .   �z  .   {     M{  �   S{  	   �{     �{  
   |     |     |     1|  
   ?|     J|  B   W|  j   �|     }     
}  =   }     X}     i}  
   w}     �}     �}  ;   �}  H   �}  7   ~     R~     n~  .   �~     �~     �~     �~  %   �~     �~  
                  $     +     4     G  �   `     �      �     �     �  	   ;�  	   E�     O�     g�     k�  B   w�     ��  S   À  A   �     Y�     l�     |�     ��     ��  ,   ǁ  !   �  (   �     ?�  #   W�  *   {�     ��  +   ��  (   �  )   
�     4�     B�     J�     X�     t�  E   ��     ȃ     ߃  $   ��  =   �  C   S�     ��     ��  
   Ä     ΄  3   �     �     ,�  .   1�     `�  "   r�  )   ��     ��     υ      ޅ     ��     �  .   �     L�     k�     ~�     ��     ��     ��     ��     ǆ     ߆     ��     �     �     $�     2�     :�  %   G�     m�     v�     ~�  �   ��  @   7�  �   x�  G  O�     ��     ��  *   Ȋ     �  
   ��     �     �  S   !�     u�  '   |�  3   ��  1   ؋     
�  2   �     J�     j�     ��  :   ��  P   ی  %   ,�  &   R�     y�     �     ��  
   ��     ��     ��  '   ��  >   �     "�     6�  &   B�     i�     |�     ��     ��     ��     Ŏ  4   ˎ  -    �  /   .�     ^�     y�  !   ��     ��  #   ��     �  (   �  �   �  C   ��  _   ��  0   Y�  4   ��  �   ��     V�     v�  e   ��     ��  w   �  [  ��  y   �  �   \�  G  �  u   3�  �   ��  �   -�  (   Ҙ  �  ��  Y   ��  )   �  '   �  7   :�  /   r�     ��     ��     ��  `   ʛ  '   +�  1   S�     ��     ��     ��     ��  @   Ҝ     �  #   (�  #   L�  )   p�  0   ��  %   ˝     �  4   
�     ?�  $   D�     i�  <   ��     ��  5   ݞ  3   �  P   G�  (   ��  Z   ��  �   �     ֠  �   ��  $   ��  
   �  *   �     �  
   �     (�     :�     L�     f�  ~   |�  &   ��     "�     +�     <�     P�     d�     x�      ��     ��  9   ȣ     �     �      �     /�     ?�     F�  
   T�  =   _�  >   ��  P  ܤ     -�  `   ��  3   �  '   B�  p   j�  �   ۧ  )   i�  $   ��  2   ��  -   �  C   �  n   ]�  Q   ̩  X   �  w   w�  $   �  m   �  /   ��  �   ��  s   6�  ,   ��     ׬     �  !   �     �     (�     .�     <�     K�     \�     i�     n�     s�  ^   |�     ۭ  2   ߭      �  =   3�  )   q�  X   ��  *   ��  3   �     S�     Y�     i�     ��     ��  ,   ��     ʯ  
   ֯     �  
   �     ��     �     �     �     (�     6�     C�     [�  #   k�  	   ��     ��     ��     ԰  "   �     �     )�     D�     `�     y�     ��     ��     α  D   ߱  E   $�  ,   j�  v   ��     �     (�     -�     >�     S�     e�  _   s�     ӳ  "   �  (   �     =�     J�     R�      g�  e   ��  �   �  h   ��  U   �  +   g�  ,   ��     ��     ۶     �     �     ��     ��     �     -�     F�  D   f�     ��     ��  @   ٷ  %   �      @�  ^   a�  K   ��  &   �     3�     I�  &   `�     ��     ��     ��     ��     ��     ��  �   ù     �      ��  �   ��  #   P�     t�  �   ��     5�  G   =�     ��     ��     ��     ɼ  x   ܼ  �   U�  &   ׽  #   ��  !   "�     D�     _�     v�     ��     ��  ~   þ     B�     [�     b�  N   {�  "   ʿ  ,   ��     �  �  +�  p   ��     7�     R�  &   l�  Z  ��  Z  ��  �  I�  V   ��  :   1�  �  l�  �   C�  �   ��  �   ��  �  2�  n   ��  E   3�  �   y�     /�     E�     Y�     _�     q�     ��  /   ��     ��     ��  s   ��  2   n�  F   ��  1   ��    �  $   �  q   D�  d   ��  "   �     >�     X�  #   i�  H  ��  �   ��  �   ��     ��     ��     ��     �  p   (�  1   ��  /   ��  )   ��  `   %�  A   ��  ^   ��  k   '�     ��     ��  '   ��     ��  >   ��  I   2�  k   |�  �   ��  N   �  �   ��     ��  k   ��  �   �  Y   ��  
   �  
    �     +�  &   8�  
   _�     j�     ��     ��     ��     ��  /   ��  j   �  6   ��  �   ��  �   I�  k   ��  3   :�  Q   n�  5  ��  
   ��  4   �  �   6�  �   �    �     �  �    �  1   ��     ��  �   ��  U   ��  
   �  |    �  �   ��  =   F�  2   ��  g   ��  l   �  �   ��  2   U�  3   ��  M   ��  [   
�  d   f�  M   ��  ]   �  F   w�  +   ��  �  ��  3   }�  <   ��  4   ��  E   #�    i�  (   ��  I   ��     ��     �     �  )   :�  �   d�  #   �  )   0�  Z   Z�  �   ��    ��  ^   ��     ��  $   �     7�  5   T�  &   ��  (   ��  l   ��  C   G�  -   ��     ��  1   ��  �     �   �     � "   � "       > )   M l   w    � 1   �    /    C    ]    y a   � *   �     b   4 �   � 7   S 7   �    � i   �    J 2   Y G   � 4   � f   	 0   p 7   � d   �    > +   [    �    �    � S   �    #	 i   6	 a   �	 2   
 q   5
 <   �
    �
    �
 0       D 2   S h   � 9   � 6   ) $   ` :   � :   � ,   � 4   ( 5   ]    � '   � "   � M   �    ? <   V 
   � #   �    �    � 3   � 1    ?   G W   � i   � �   I �   � �   Z    � @    <   [ 9   �    � G   � E   4 E   z F   � $    %   ,    R 0   n C  �    �    � [       ] (   i 8   � �   � A   � 
   �    � &    E   7 #   } D   �    � }   �    y    �    �    �    � "   � $    ,   >    k !   r "   �    � !   �    �    �    
 K    *   _ U   � ^   �    ? *   Q �   | �  & �   �    � =   � ?     U   V     �     �     �     !    ! S   6! �   �! �   "    �" H   �" �   *# "   &$ *   I$ V   t$ T   �$ @    % 0   a% r   �% T   & �   Z& A   .' :   p' I   �' 7   �' H   -( 8   v( 9   �( 1   �( 3   )    O)    ^)    k)    y) 4   �) '   �) F   �) )   **    T*    o* �   �* a   )+ �   �+ �   +,   �, !  �. '   �/ �    0 �   �0 u   c1 2   �1 c   2 A   p2    �2 '   �2 #   �2 Q   3 ,   j3    �3    �3 w   �3    ?4 :   Z4    �4 '   �4 r   �4 �  P5 �   �6 ;   �7 $   �7 p   �7 D   ^8    �8    �8    �9    �9    :    : 6   .:    e:    �:    �: n   �: �   ;    �; 0   �; k   < &   �< 3   �<    �<    �<    �< V   = �   ^= R   �= @   >> @   > {   �> 3   <?    p? '   w? W   �? 
   �?    @ +   @    B@    O@    d@ +   v@    �@   �@ !   �A    �A    B ?   /B    oB    �B ,   �B    �B    �B �   �B    C |   �C �   D !   �D !   �D @   �D ?   )E =   iE X   �E B    F X   CF ,   �F =   �F T   G    \G V   uG @   �G \   H    jH    �H    �H 7   �H    �H �   I #   �I $   �I =   �I �   J �   �J    )K 6   HK    K /   �K j   �K :   -L    hL n   �L 1   �L N   (M m   wM /   �M #   N F   9N +   �N ,   �N c   �N 1   =O H   oO *   �O 8   �O    P    )P !   8P )   ZP <   �P    �P W   �P    2Q    QQ    hQ    wQ A   �Q    �Q    �Q H   �Q B  8R �   {S �  T �  �U 5   ~X '   �X Y   �X    6Y    CY 7   \Y    �Y �   �Y    RZ >   aZ W   �Z y   �Z +   r[ h   �[ =   \ 8   E\ 5   ~\ ]   �\ \   ] Z   o] Z   �] 
   %^ !   0^ 2   R^    �^    �^ "   �^ P   �^ v   '_ ,   �_    �_ b   �_ ,   F` 6   s` 8   �`    �` #   �` 	   a    "a M   �a e   �a 3   Vb    �b X   �b 0   �b =   )c (   gc n   �c O  �c ~   Oe �   �e ~   �f p   g U  �g g   �h    Li �   gi    fj �   }j   rk &  �n G  �o ~  �p �   |s   bt 3  iu `   �v   �v �   z J   �z H   { :   T{ >   �{ ;   �{    
|    | �   /| R   %} V   x} *   �} )   �} ,   $~ 3   Q~ �   �~ %   4 F   Z L   � U   � X   D� Y   �� :   �� �   2�    �� T   ȁ 8   � z   V� ;   т b   � f   p� �   ׃ T   h� �   �� �  f� .   � �  "� @   �� 
   � U   �    H�    V� %   k�    ��    �� 8   ��   �� �   �    ��    �� $   �� -   � /   � #   @� *   d� :   ��    ʌ 0   J�    {�    ��    ��        ɍ    � x   � �   |� �  
� �    �   �� T   E� I   �� �   �   �� 5   �� =   � H   .� U   w� g   ͖ �   5� v   � �   ^� �   %� ,   � �   2� n   ؚ �   G� �   &� ^   � 0   G�    x� D   �� 8   Ν    �    �    5�    G�    e�    t�    y�    �� �   ��    *� d   .� E   �� x   ٟ M   R� �   �� R   �� i   ء    B� "   ^� 2   �� .   ��    � u   � #   ]�    �� %   ��    �� 
   ѣ    ܣ !   �    �    4�    N� >   d� '   �� ?   ˤ 	   � %   �    ;� #   [� .   �    �� +   Υ -   �� (   (� !   Q�    s�    ��    �� l   ˦ j   8� A   �� �   � 6   Ĩ    �� 8   
� 6   C� '   z� )   �� �   ̩ V   �� b   � 7   h� ,   ��    ͫ ,   � >   � �   V� -  � �   � �   Ů K   H� U   �� 5   � -    �    N�    [�    i� 2   n� $   �� /   ư 2   �� s   )�    ��    ��    ˱ o   K� 1   �� �   �� {   �� e   (� "   ��    �� /   ϴ    �� 
   �    �    (� "   ,� (   O� �  x� C   5� I   y� _  ÷ ]   #� )   �� �  ��    2� �   E� 6   � F   � 0   c� 6   �� �   ˼ �   Ƚ F   �� M   � T   =� C   �� <   ֿ 6   � C   J� :   �� )  �� 9   ��    -� *   :� ~   e� :   �� n   �    ��                     �           �  �  �  #  7   4  �  �   �  �  =  <          s  �   �  �   �      x   �  �  �  �   �      k  �  �   +  �      �           X  �  �   �  �  X     l  �     S  �  �  �  r  �  �  g  |  �  �   �  �   �   #   u  j  0        �   �  b  5     �          u   �           �  �      @     �   R       o  �  �   i      �   |      a     �  ,   �  �      �  �  �  �               o                        �   \  V  �      �  �          >       �   W  �  V        6      �   .      �      �  :          ^  �   $         �  G  �  �  �       �  X  �  ^      \  -          q  /          L  �        �   #        �  >  �     3  �  Q    �  �   �      �         �       �   �               E   .  W          �    �  ~  �  H   �   �          �  0  O  �   4  D  .       �        �    �    �                ?  I  �  �      i   w      d      �       �      �   H      �  �  �   "     �    %  �      �  �  �   �  -  �  �      U  �   �  N        �        F   �      {  �       �  `     �   4       G   I   �               �       h      ;   �      �  �  �   �   �   �   _         �   �  �  �  L    �  �      �  	   �  )   �  f       �             �          �  J   *       e   �  �  �  �          }  l   �  �             �   t  �   �    I  c  R      �  �   �   �  �      �      �      p  C      �   �        �  (    �  �  2       �  h   �      �  �  &   o  �  �  �  &  �          x     �  p               �  �   �          `  �   �          �    }  ]        �              �   M      -  \       5  �      �  A  �     =   8       ~   �             �  �   �       �       �  �   �   �  S  �   �  J  �  8  �      �   F  &  �   N   �           *  /       �       q  [  R  �   (   Y  p              �  �  r   �      :    @   U  n      �   �   �   �  [       �          �  �  �       �  3  �  =  �   �   �  T      !           �  �   �          %   �   �  ,      f     �   �  8  G  "   �  �  �  d      �   �  t   �  �   �   �  �      K  y     Y      �   z   {   �        �       �  �          H  !  �   �     n   �  �       �          ^               1       M   /    
         >  �  �   %  *  �  E  :   5            Z               �   �      $   v  m  V   N  �   K  {         g            �  �      S   k   �     B  
  <   �          �  �  �  �          D  �  �  $  Y      �  �  ]   +           �  s   L   �   ;      B       �  ;          �      �  )  
   _  !      k  w  y  �  �  m          �   �  7        �  �            �   �       2  @  '   y  0   K   �          �   Q          �     A   �  �   z  �   r  �  _      �  �     �       �   �    C      �  �  �  u      �  q   B  ~  �  �   �  a      �  �      �      �          )  h      �         Q      �   P               �   �   "  �      �  n  z  ?   x         �   O      �    9  }   �      '      '  �  �       �  b   D      �  �      �    c  �                      A  �   �    F  a  �  �  �   f  �  1  �  �      <  W  �       d       M          g   �  �  �  �  �   �      t    �  �  �   �   �             �         �    �      j  �  �  �  ?      m   e          �  �   w           �  �  2  �   Z             �  i  �          P      s          6          �   �          O      Z  7          �  �      T              �  �  �  [  ]  �  e  P  �           `          +  ,  E  �          T       �  1  C             �  �  	  �  �   |  �  �   U     �   (      J  �  9   �   �  �   �  �   	  �   �   �  �  �   9  3   �  �  v      �                      b      �  �         �    6  l  j       c               v    

New releases fix bugs and offer new features. 

This is Videomass v.{0}

 
  ...Not found 
  ..Nothing available 
- The concatenation function is performed only with Audio files or only with Video files.

- The order of concatenation depends on the order in which the files were added.

- The output file name will have the same name as the first file added (also depends on the
  settings made in the preferences dialog or the renaming functions used).

- Video files must have exactly same streams, same codecs and same
  width/height, but can be wrapped in different container formats.

- Audio files must have exactly the same formats, same codecs with equal sample rate. 
1. Import one or more image files such as JPG, PNG and BMP formats, then select one.

2. Use the "Resizing" filter to resize images which have different sizes such as width and height. It is optional in other cases.

3. To set the DURATION between images use the "End" control on the Trim bar.

4. Start the conversion.


The produced video will have the name of the selected file in the 'Queued File' list, which will be saved in a folder named 'Still_Images'
with a progressive digit, in the path you specify. 
1. Import one or more video files, then select one.

2. To select a slice of time use the "Start" "End" controls on the Trim bar.

3. Select an output format (jpg, png, bmp).

4. Start the conversion.


The images produced will be saved in a folder named "Movie_to_Pictures" with a progressive digit, 
in the path you specify. 
First, choose a topic in the drop down list  Do you want to enable this feature? "Auto" keeps all audio stream but processes only the one of the selected index; "All" keeps all audio streams and processes them all with the properties of the selected index; "Index only" processes and keeps only the selected index audio stream. "Video" to save the output file as a video; "Audio" to save as an audio file only "cpu-used" sets how efficient the compression will be. The meaning depends on the mode above. "ffmpeg", "ffprobe" and "ffplay" are required. Complete all
the text boxes below by clicking on the respective buttons. "good" is the default and recommended for most applications; "best" is recommended if you have lots of time and want the best compression efficiency; "realtime" is recommended for live/fast encoding # It will be replaced by increasing numbers starting with: 'Successful!

A new empty preset has been created. '{}' is not installed on your computer. Install it or indicate another location by clicking the 'Locate' button. ...Finished ...Interrupted 180° 90° (left) 90° (right) < Previous =  Below max peak =  Clipped peaks =  No changes A file with this name already exists, do you want to overwrite it? A long description of the profile A new preset was successfully imported A new release is available - v.{0}
 A set of useful tools for audio and video conversions. Save your profiles and reuse them with Presets Manager. A short profile name A target file must be selected in the queued files A useful tool to search for FFmpeg help topics and options A/V Conversions	Shift+V AV-Conversions Abort About Videomass Activate RMS-based normalization, which according to mean volume calculates the amount of gain to reach same average power signal. Activate peak level normalization, which will produce a maximum peak level equal to the set target level. Activate two passes normalization. It Normalizes the perceived loudness using the "​loudnorm" filter, which implements the EBU R128 algorithm. Activates row-mt 1 Add Add Files Add Profile Add a new profile from this panel with the current settings Add metadata to file Additional arguments Advanced Options All default presets have been successfully recovered All presets have been exported successfully Already exist: 

{}

Do you want to overwrite?  An item must be selected in the URLs checklist Appearance Application Language Application preferences Apply Apply Denoisers Filters Are you sure to empty trash folder? Are you sure you want to clear the selected log file? Are you sure you want to delete the selected profile? It will no longer be possible to recover it. Are you sure you want to exit? Are you sure you want to remove "{}" preset?

 It will be moved to the "Removals" subfolder of the presets folder. Aspect Ratio Assign optional suffix to output file names: At least one "Format Code" must be checked for each URL selected in green. At least two files are required to perform concatenation. At the bottom of window At the left of window At the right of window At the top of window (default) Audio Audio Codec Audio Encoder Audio Filters Audio Properties Audio Streams Audio Streams Mapping Audio normalization is required only for some files Audio normalization off Audio normalization processes cannot be saved on the Presets Manager. Audio normalization will not be applied because the source signal is equal Auto-create subfolders when downloading playlists Auto-detection Auto-exit after playback Available video codecs. "Copy" is not a codec but indicate that the video stream is not to be re-encoded and allows changing the format or other parameters Back Batch renaming	Ctrl+B Be careful! The selected preset will be overwritten with the default one. Your profiles may be deleted!

Do you want to continue? Be careful! This action will restore all presets to default ones. Your profiles may be deleted!

Do you want to continue? Bit depth is the number of bits of information in each sample, and it directly corresponds to the resolution of each sample. Bit depth is only meaningful in reference to a PCM digital signal. Non-PCM formats, such as lossy compression formats, do not have associated bit depths. Box Color By assigning an additional suffix you could avoid overwriting files CODING CAPABILITY Cache folder Cannot continue: The duration in the timeline exceeds the duration of some queued files. Cannot save current data in file '{}'. Center Change the size and color of the timestamp during playback Changes will take effect once the program has been restarted.

Do you want to exit the application now? Check for new presets Check for newer version Check for the latest Videomass version at <https://pypi.org/project/videomass/> Check new versions of Videomass presets from homepage Check the current output or read the related log file for more information. Checking for newer version Choose a download folder Choose a folder to export all presets Choose a folder to save the selected preset Choose a new destination for the files to be trashed Choose a preset and view its profiles Choose a temporary destination for conversions Choose a temporary destination for downloads Choose a topic in the list Choose an index from the available audio streams. If the source file is a video, it is recommended to select a numeric audio index. If the source file is an audio file, leave this control to "Auto". Choose the {} executable Clear all enabled filters  Clear selected log Clear the cache when exiting the application Click on "Playlist Items" column to specify indices of the videos in the playlist separated by commas like: "1,2,5,8" if you want to download videos indexed 1, 2, 5, 8 in the playlist.

You can specify range: "1-3,7,10-13" it will download the videos at index 1, 2, 3, 7, 10, 11, 12 and 13.
 Click the "Next" button Click the "Next" button to get started Close Videomass Columns: Concatenate Demuxer	Shift+D Concatenate media files Concatenate multiple media files based on import order without re-encoding Configuration folder Confirm Settings Congratulation! You are already using the latest version.
 Consider transforms as relative to previous frame if checked, absolute if unchecked. Default is checked. Constant rate factor. Lower values = higher quality and a larger file size. Set -1 to disable this control. Constrain proportions (keep aspect ratio) Container: Conversions folder	Ctrl+C Convert Convert using FFmpeg Create a new preset Create a new profile Create a new profile and save it in the selected preset Create a new profile on "{}" preset Create animated GIF Create thumbnails Create tiled mosaics Create video from a sequence of images, based on import order, with the ability to add an audio file. Create, edit and use quickly your favorite FFmpeg presets and profiles with full formats support and codecs. Crop Filter Crop to height Crop to width Cropping Cropping area selection  Cross-platform graphical interface for FFmpeg and youtube-dl.
 Current path: DECODING CAPABILITY DISABLED: Data Format Deadline/Quality Decoders Default destination folders successfully restored Default position Deinterlace Deinterlace the input video with `w3fdif` filter Deinterlace the input video with `yadif` filter. Using FFmpeg, this is the best and fastest choice Deinterlaces with w3fdif filter Deinterlaces with yadif filter Deinterlacing Deinterlacing and Interlacing Delete Delete all files from the list Delete all files in the Videomass Trash folder Delete all text from the list Delete the contents of the log files when exiting the application Delete the selected profile Demuxing only Demuxing/Muxing support Denoiser Denoiser filters Denominator Description Destination folder Directories are not allowed, just add files, please. Disabled Display Aspect Ratio. Set to 0 to disable Display of selected items in text format Displays timestamp Displays timestamp when playing movies with FFplay Don't show this dialog again Donation Done! Don’t check SSL certificate Download Download Audio only Download all Videomass presets locally from the homepage Download all available subtitles Download all videos in playlist Download by format code Download files using youtube-dl Download split audio and video Download subtitles only Download the latest presets Download videos by resolution Downloader Downloader preferences Downloads folder	Ctrl+D Drag one or more files below Duplicate Duplicate files are rejected: > {} Duration Duration: ENABLED: ERROR ERROR: Invalid URL: "{}" ERROR: Invalid option ERROR: Missing audio stream:
"{}" ERROR: Some equal URLs found ERROR: Typing error on JSON keys: {}

File: "{}"
key malformed ? ERROR: Unable to get format codes.

Unsupported URL:
'{0}' ERROR: {0}

{1} is not installed, use your package manager to install it. Easily download videos and audio in different formats and quality from YouTube, Facebook and more sites. Edit Edit profile of the "{}" preset Edit the selected profile Embed thumbnail in audio file Empty Trash Enable a single still image Enable another location to run FFmpeg Enable another location to run FFplay Enable another location to run FFprobe Enable hqdn3d denoiser Enable nlmeans denoiser Enable stabilizer Enable text format Enable virtual tripod mode if checked, which is equivalent to relative=0:smoothing=0. Default is unchecked. Use also tripod option of vidstabdetect. Enabled Encoders End segment in 24-hour format (HH:MM:SS) End: Enter URLs below Enter name for new preset Enter only alphanumeric characters. You can also use the hyphen ("-") and the underscore ("_"). Spaces are not allowed. Error, invalid preset: "{}" Exit Exit	Ctrl+Q Export all... Export entire presets folder as copy Export selected Export selected preset as copy Extension Extract images (frames) from your movies in JPG, PNG, BMP, GIF formats. FFmpeg FFmpeg configuration FFmpeg decoders FFmpeg documentation FFmpeg encoders FFmpeg file formats FFmpeg help topics FFmpeg logging levels FFplay FFplay   ...not found ! FFprobe   ...not found ! FILE SELECTION Fatal Error ! File File Name File Preferences File does not exist or is invalid:  %s File does not exist:

"{}"
 File has illegal characters like: File list changed, please check the settings again. File name File renaming... File to concatenate
Output filename
Destination
Output Format
Time Period File to process
Output filename
Destination Folder
Output Format
Additional arguments
Audio file
Shortest
Resizing
Pre-input
Frame per Second (FPS)
Still image duration
Overall video duration File without format extension: please give an appropriate extension to the file name, example '.mkv', '.avi', '.mp3', etc. Finish First pass of the selected profile First select a profile in the list First, choose a topic in the drop down list Flags disabled Flags enabled Flags settings Font Color Font Size For more details, see the Videomass User Guide: For more information, visit the official FFmpeg documentation: Force original aspect ratio using
padding rather than stretching Format Code Formats must be comma-separated Frames repeat a given number of times per second. In some countries this is 30 for NTSC, other countries (like Italy) use 25 for PAL From Movie to Pictures From Movie to Pictures	Shift+S From an image sequence to a video file Gathers information of multimedia streams Generates duo video for comparison Get Latest Version Get version about your operating system, version of Python and wxPython. Get your files at the destination you specified Gets maximum volume and average volume data in dBFS, then calculates the offset amount for audio normalization. Go to the 'A/V Conversions' panel Go to the 'Concatenate Demuxer' panel Go to the 'From Movie to Pictures' panel Go to the 'Home' panel Go to the 'Presets Manager' panel Go to the 'Still Image Maker' panel Go to the 'YouTube Downloader' panel Go to the next panel Go to the previous panel Goto Height Height: Help Help viewer Home panel	Shift+H How to install pip on your Linux distribution How to upgrade a Python package Icon size: Icon themes If FFmpeg is not on your computer, this application will be unusable If activated, hides some output messages. If checked, the FFplay window will auto-close at the end of playback If you have already installed FFmpeg on your operating
system, click the "Auto-detection" button. If you want to keep the aspect ratio, select "Constrain proportions" below and
specify only one dimension, either width or height, and set the other dimension
to -1 or -2 (some codecs require -2, so you should do some testing first). If you want to use a version of FFmpeg located on your
filesystem but not installed on your operating system,
click the "Locate" button. Ignore case Ignore case distinctions: characters with different case will match. Images need to be resized, please use Resizing function. Import a group of presets from a folder and update existing ones Import a new preset Import a new preset or update an existing one Import a new presets folder Import group Import preset Include audio file Include the video ID
in the file names Incomplete profile assignments Index Selection: Informations Integrated Loudness Target in LUFS. From -70.0 to -5.0, default is -24.0 Interlace Interlaces with interlace filter Interrupted Process ! Invalid file: '{}' Invalid path name. Contains double or single quotes Invalid preset loaded.
It is recommended to remove it or rewrite it into a JSON format compatible with Videomass.

Possible solution: open the Presets Manager panel, go to the presets column and try to click the "Restore" button Invert transforms if checked. Default is unchecked. Is 'ffmpeg' installed on your system? Issue tracker It can reduce the file size, but takes longer. Keeps track of the output for debugging errors Level Limiter for the maximum peak level or the mean level (when switch to RMS) in dBFS. From -99.0 to +0.0; default for PEAK level is -1.0; default for RMS is -20.0 Listening Load Load Frame Locate Locating FFmpeg executables
 Log file list Log folder Log messages Loudness Range Target in LUFS. From +1.0 to +20.0, default is +7.0 Make sure you are using the latest available version of
'{}'. This allows you to avoid download problems.
 Map: Max volume dBFS Maximum True Peak in dBTP. From -9.0 to +0.0, default is -2.0 Mean volume dBFS Media Streams Media type Media: Miscellanea Move horizontally - set to -1 to center the horizontal axis Move source file to the Videomass trash folder after successful encoding Move vertically - set to -1 to center the vertical axis Multimedia streams analyzer Muxers and Demuxers Muxers and demuxers available for used FFmpeg. Muxing only Name Name already in use: Name has illegal characters like: {0} New New Name # New size in pixels Next Next > No Audio No files to delete No new version available No presets found.

Possible solution: open the Presets Manager panel, go to the presets column and try to click the "Restore all..." button No such folder '{}' Normalization Not Installed Not everything was successful. Not found Numerator OK: Indexes to download Off Offset dBFS One or more comma-separated format names to include in the profile One-Pass One-Pass, Do not start with `ffmpeg -i filename`; do not end with `output-filename` Only one video can be played at a time.

Unsupported '{0}':
'{1}' Open an audio file Open audio file Open one or more files Open temporary conversions Open temporary downloads Open the Videomass Trash folder if it exists Open the default downloads folder Open the default file conversions folder Open the selected files Open the temporary downloads folder Open the temporary file conversions folder Open...	Ctrl+O Opens the Videomass cache folder, if exists Opens the Videomass configuration folder Opens the Videomass log folder, if exists Optimizations Options Other options Other unofficial resources: Output Format Output Format. Empty to copy format and codec. Do not include the `.` Output Monitor	Shift+O Output file name Output folder does not exist:

"{}"
 Output format extension. Leave empty to copy codec and format Output format. It also represents the extension of the output file. Output format: PEAK-based volume statistics Parameters Path to the executables Permission denied: {}

Check execution permissions. Place the toolbar Play Play and listen to the result of audio filters Play selected url Play the selected file in the list Play the video until audio track finishes Playlist Editor Playlist Items Playlist video items to download Please Confirm Please confirm Please take a moment to set up the application Post-normalization references: Precompiled Videos Preferences	Ctrl+P Preferred video format Preset Presets Presets Manager Presets Manager	Shift+P Prevent overwriting files Preview Preview video filters Process log: Processing... Profile Profile Name Profile already stored with same name Profiles Quality Quality/Speed
ratio modifier: Queued File
Output Format
Web Optimize
Audio Codec
Audio bit-rate
Audio Channels
Audio Rate
Bit per Sample
Audio Normalization
Time Period
Input Audio Map Queued File
Pass Encoding
Profile Used
Output Format
Time Period Queued File
Web Optimize
Output Format
Video Codec
Aspect Ratio
FPS
Audio Codec
Audio Channels
Audio Rate
Audio bit-rate
Bit per Sample
Audio Normalization
Input Audio Map
Output Audio Map
Subtitles Map
Time Period Queued File
Web Optimize
Pass Encoding
Output Format
Video Codec
Video bit-rate
CRF
Min Rate
Max Rate
Buffer size
VP8/VP9 Options
Video Filters
Aspect Ratio
FPS
Preset
Profile
Tune
Audio Codec
Audio Channels
Audio Rate
Audio bit-rate
Bit per Sample
Audio Normalization
Input Audio Map
Output Audio Map
Subtitles Map
Time Period RMS-based volume statistics Re-start is required Read and write useful notes and reminders. Ready References Refresh all log files Reload Remember that you can always change these settings later, through the Setup dialog. Remove Remove the selected files from the list Remove the selected preset from the Presets Manager Rename all files listed in the Queued Files panel Rename items Rename the file selected in the Queued Files panel Rename the selected file	Ctrl+I Rename the selected file to: Rename the {0} items to: Replace the selected preset with the Videomass default one Required: Microsoft Visual C++ 2010 Service Pack 1 Redistributable Package (x86) Reserved arguments for the first pass Reserved arguments for the second pass Reset Resizing Resizing filters Resolution Restore Restore all... Restore the default destination folders Restore the default folders for file conversions and downloads Restrict file names Result dBFS Retrieve all Videomass default presets Rotate 180 degrees Rotate 90 degrees left Rotate 90 degrees right Rotation Rotation setting Rows: Sample (aka Pixel) Aspect Ratio.
Set to 0 to disable Save all downloads to this temporary location Save each file in the same folder as input file Save the new profile to... Save.. Scale filter, set to 0 to disable Search for a specific frame Second pass of the selected profile Select a log file Select items to view them in text format Selected File
Output Format
Destination Folder
Rate (fps)
Resizing
Mosaic rows
Mosaic columns
Mosaic padding
Mosaic margin
Custom Arguments
Time Period Selected index does not exist or does not contain any audio streams Set FPS control from 0.1 to 30.0 fps. The higher this value, the more images will be extracted. Set a persistent location to save exported files Set a persistent location to save the file downloads Set how shaky the video is and how quick the camera is. A value of 1 means little shakiness, a value of 10 means strong shakiness. Default value is 5. Set integrated loudness target: Set loudness range target: Set maximal angle in radians (degree*PI/180) to rotate frames. Default value is -1, meaning no limit. Set maximum true peak: Set minimum contrast. Below this value a local measurement field is discarded. The range is 0-1. Default value is 0.25. Set optimal zooming to avoid borders. Accepted values are: `0` disabled, `1` optimal static zoom value is determined (only very strong movements will lead to visible borders) (default), `2` optimal adaptive zoom value is determined (no borders will be visible), see zoomspeed. Note that the value given at zoom is added to the one calculated here. Set percent to zoom maximally each frame (enabled when optzoom is set to 2). Range is from 0 to 5, default value is 0.25. Set percentage to zoom. A positive value will result in a zoom-in effect, a negative value in a zoom-out effect. Default value is 0 (no zoom). Set reference frame number for tripod mode. If enabled, the motion of the frames is compared to a reference frame in the filtered stream, identified by the specified number. The idea is to compensate all movements in a more-or-less static scene and keep the camera view absolutely still. The frames are counted starting from 1. Set stepsize of the search process. The region around minimum is scanned with 1 pixel resolution. Default value is 6. Set the accuracy of the detection process. A value of 1 means low accuracy, a value of 15 means high accuracy. Default value is 15. Set the camera path optimization algorithm. Values are: `gauss`: gaussian kernel low-pass filter on camera motion (default), and `avg`: averaging on transformations Set the interlacing filter coefficients. Set the number of frames (value*2 + 1) used for lowpass filtering the camera movements. Default value is 15. For example a number of 10 means that 21 frames are used (10 in the past and 10 in the future) to smoothen the motion in the video. A larger value leads to a smoother video, but limits the acceleration of the camera (pan/tilt movements). 0 is a special case where a static camera is simulated. Set the time interval between images in seconds (from 1 to 100 sec.), default is 1 second Set up a temporary folder for conversions Set up a temporary folder for downloads Setdar filter (display aspect ratio) example 16/9, 4/3  Setsar filter (sample aspect ratio) example 1/1 Setting timestamp Settings Shadow Color Sharpen or blur the input video. Note the use of the unsharp filter which is always recommended. Shortcut keys while playing with FFplay Show FFmpeg's built-in configuration capabilities Show Logs	Ctrl+L Show configuration Show text box Show the latest version... Show useful shortcut keys when playing or previewing with FFplay Showing log messages Shows available decoders for FFmpeg Shows available encoders for FFmpeg Shows download statistics and information Shows the latest version available on github.com Shows the text in the toolbar buttons Shows the version in use Simple interlacing filter from progressive contents. Size Some text boxes are still incomplete Sorry, all task failed ! Sorry, this preset is not part of default Videomass presets. Source size: {0} x {1} pixels Spaces around the mosaic borders. From 0 to 32 pixels Spaces around the mosaic tiles. From 0 to 32 pixels Specifies a maximum tolerance. This is only used in conjunction with buffer size Specifies a minimum tolerance to be used Specifies the decoder buffer size, which determines the variability of the output bitrate  Specify how to deal with borders that may be visible due to movement compensation. Values are: `keep` keep image information from previous frame (default), `black` fill the border black Specify type of interpolation Specify type of interpolation. Available values are: `no` no interpolation, `linear` linear only horizontal, `bilinear` linear in both directions (default), `bicubic` cubic in both directions (slow) Specify which frames to deinterlace. Stabilizer Start segment in 24-hour format (HH:MM:SS) Start: Statistics Statistics viewer Still Image Maker Still Image Maker	Shift+I Stops current process Stream duration always refers to the file with the longest duration.
If the "Duration" data is missing, it will be set to {0}. Stream duration. Click me for details. Subtitle Subtitle Streams Successful changes! Successful recovery Successful storing! Successfully completed ! Successfully downloaded to "{0}" Supported Format List Supported Formats list (optional). Do not include the `.` Suppress excess output System System version TITLE SELECTION Target Target level: Thank You! The URLs contain channels. Are you sure you want to continue? The URLs contain playlists. Are you sure you want to continue? The audio Rate (or sample-rate) is the sound sampling frequency and is measured in Hertz. The higher the frequency, the more true it will be to the sound source and the more the file will increase in size. For audio CD playback, set a sampling frequency of 44100 kHz. If you are not sure, set to "Auto" and source values will be copied. The audio bitrate affects the file compression and thus the quality of listening. The higher the value, the higher the quality. The chosen icon theme will only change the icons,
background and foreground of some text fields. The downloader is disabled. Check your preferences. The file is not a frame or a video file The files do not have the same "codec_types", same "sample_rate" or same "width" or "height". Unable to proceed. The following settings affect output messages and
the log messages during transcoding processes.
Change only if you know what you are doing.
 The preset "{0}" was successfully removed The preset was exported successfully The presets database has been successfully updated The process was stopped due to a fatal error. The search function allows you to find entries in the current topic The selected profile command has been changed manually.
Do you want to apply it during the conversion process? The selected profile is not suitable to convert the following file formats:

%s

 The timestamp size does not auto-adjust to the video size, you have to set the size here There are still processes running.. if you want to stop them, use the "Abort" button.

Do you want to kill application? There is a new version available {0} This feature allows you to download video and audio from
many sites, including YouTube.com and even Facebook. This filter is incompatible with 2-pass enabled This preset already exists and is about to be updated. Don't worry, it will keep all your saved profiles.

Do you want to continue? This will update the presets database. Don't worry, it will keep all your saved profiles.

Do you want to continue? Threads used for transcoding (from 0 to 32): Timestamp settings Title To exit click the "Finish" button Toolbar customization Tools Topic List... Translation... Transpose Filter Trash folder Trim Tune Two-Pass Two-Pass (optional), Do not start with `ffmpeg -i filename`; do not end with `output-filename` URL URL list changed, please check the settings again. URLs have no playlist references Unable to get format codes on '{0}'

Unsupported '{0}':
'{1}' Unable to preview Video Stabilizer filter Undetected volume values! Click the "Volume detect" button to analyze audio volume data. Unexpected error while creating file:

{0} Unexpected error while deleting file contents:

{0} Unset Unsharp filter: Unsupported format '{}' Update the presets list Url Use a temporary location to save conversions Use for Web User Guide Version in Use Version {} Video Video Codec Video Detect Video Encoder Video Filters Video Stream Video stabilizer filter Video transform Video width and video height ratio. Videomass Videomass - AV Conversions Videomass - Concatenate Demuxer Videomass - Downloading... Videomass - From Movie to Pictures Videomass - Loading... Videomass - Output Monitor Videomass - Presets Manager Videomass - Queued Files Videomass - Queued URLs Videomass - Still Image Maker Videomass - YouTube Downloader Videomass Wizard Videomass already seems to include FFmpeg.

Do you want to use that? Videomass has a simple graphical
interface for youtube-dl and yt-dlp
 Videomass is an application based on FFmpeg
 Videomass supports mono and stereo audio channels. If you are not sure set to "Auto" and source values will be copied. Videomass: Please Confirm View Viewing last log Viewing log messages Volume Statistics Volume detect WARNING: The selected URL does not refer to a playlist. Only lines marked green can be indexed. Wait....
Audio peak analysis. Wait....
Retrieving required data. Wait....
The archive is being downloaded Warn on exit Warning Welcome to Videomass Welcome to the Videomass Wizard! When it's grey...

...It means the resulting audio will not change,
because it's equal to the source. When it's red...

...It means the resulting audio will be clipped,
because its volume is higher than the maximum 0 db
level. This results in data loss and the audio may
sound distorted. When it's yellow...

...It means an audio signal will be produced with
a lower volume than the original. When not available, the chosen video resolution will be replaced with the closest one Where do you prefer to save your downloads? Where do you prefer to save your transcodes? Which dimension to adjust? While playing Width Width: Wiki Wizard completed successfully!
 Work Notes	Ctrl+N Write subtitles to video You are using '{0}' version {1} You are using a development version that has not yet been released!
 YouTube Downloader YouTube downloader	Shift+Y [CROP] Specify how to deal with borders at movement compensation [INVERT] Invert transforms if checked [OPTALGO] optimization algorithm [RELATIVE] Consider transforms as relative to previous frame if checked, absolute if unchecked [TRIPOD2] virtual tripod mode, equivalent to relative=unchecked:smoothing=0 [TRIPOD] Enable tripod mode if checked [Videomass]: FAILED ! [Videomass]: SUCCESS ! cache folder has not been created yet. description flags format fps help topic list hqdn3d options hqdn3d:
This is a high precision/quality 3d denoise filter. It aims to reduce image noise, producing smooth images and making still images really still. It should enhance compressibility. list sinks of the output device list sources of the input device lowpass:
Enable (default) or disable the vertical lowpass filter to avoid twitter interlacing and reduce moire patterns.
Default is no setting. mode
The interlacing mode to adopt. nlmeans options nlmeans:
Denoise frames using Non-Local Means algorithm is capable of restoring video sequences, even with strong noise. It is ideal for enhancing the quality of old VHS tapes. options parity
The picture field parity assumed for the input interlaced video. please try again. print all options (very long) print basic options print more options q, ESC
f
p, SPC
m
9, 0
/, *
a
v
t
c
w
s

left/right
down/up
page down/page up

right mouse click
left mouse double-click scan:
determines whether the interlaced frame is taken from the even (tff - default) or odd (bff) lines of the progressive frame. show available HW acceleration methods show available audio sample formats show available bit stream filters show available color names show available devices show available filters show available pixel formats show available protocols specifies the target (average) bit rate for the encoder to use. Higher value = higher quality. Set -1 to disable this control. start  {} | duration  {} status wait... I'm interrupting wait... all operations will be stopped at the end of the download in progress. {0}: Latest version available: {1} {}

Sorry, removal failed, cannot continue.. {} file in queue Project-Id-Version: Videomass 4.0.5
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-01-19 18:19+0100
Last-Translator: ChourS <ChourS2008@yandex.ru>
Language-Team: Russian (RU)
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Generated-By: pygettext.py 1.5
X-Generator: Poedit 2.4.2
X-Poedit-SourceCharset: UTF-8
 

Новые выпуски исправляют ошибки и предлагают новые функции. 

Это Videomass v.{0}

 
  ...Не найдено 
  ..Данные недоступны 
- Функция конкатенации(последовательного соединения) выполняется только с аудиофайлами или только с видеофайлами..

- Порядок объединения зависит от порядка добавления файлов.

- Имя выходного файла будет таким же, как у первого добавленного файла
 (также зависит от настроек, сделанных в диалоговом окне настроек,
  или от используемых функций переименования).

- Видеофайлы должны иметь одинаковые потоки, одинаковые кодеки и одинаковые
  ширину/высоту, но может быть упакован в разные форматы контейнеров.

- Аудиофайлы должны иметь одинаковые форматы, одинаковые кодеки с одинаковой частотой дискретизации. 
1. Импортируйте один или несколько файлов изображений, таких как форматы JPG, PNG и BMP, затем выберите один из них.

2. Используйте фильтр «Изменение размера», чтобы изменить размер изображений, которые имеют разные размеры, такие как ширина и высота. В других случаях это необязательно. 

3. Чтобы установить ПРОДОЛЖИТЕЛЬНОСТЬ между изображениями, используйте элемент управления «Конец» на панели Trim.

4. Начните преобразование.


Созданное видео будет иметь имя выбранного файла в списке «Файл в очереди», который будет сохранен в папке с именем «Still_Images» 
 и с последовательным номером папки по указанному вами пути. 
1. Импортируйте один или несколько видеофайлов, затем выберите один.

2. Чтобы выбрать отрезок времени, используйте элементы управления «Начало», «Конец» на панели Trim.

3. Выберите выходной формат (jpg, png, bmp).

4. Начните преобразование.


Созданные изображения будут сохранены в папке с именем «Movie_to_Pictures» 
 с последовательным номером папки по указанному вами пути. 
Сначала выберите тему в раскрывающемся списке  Вы хотите включить эту функцию? "Авто" сохраняет весь аудиопоток, но обрабатывает только один из выбранных индексов; "Все" сохраняет все аудиопотоки и обрабатывает их все с помощью свойства выбранного индекса; "Только индекс" обрабатывает и сохраняет только аудиопоток выбранного индекса. "Видео", чтобы сохранить выходной файл как видео; "Аудио" для сохранения только как аудиофайл "cpu-used" устанавливает эффективность сжатия. Это зависит от указанных выше условий. "ffmpeg", "ffprobe" и "ffplay" обязательны. Заполните все
текстовые поля ниже, нажав соответствующие кнопки. "good" является значением по умолчанию и рекомендуется для большинства приложений; "best" рекомендуется, если у вас много времени и вы хотите лучшую эффективность сжатия; "realtime" рекомендуется для живого/быстрого кодирования # Имя будет заменено возрастающими числами, начинающимися с: Успешно!

Создана новый пустой пресет. '{}' не установлен на вашем компьютере. Установите его или укажите другое место, нажав кнопку 'Найти'. ...Завершено ...Прервано 180° 90° (влево) 90° (вправо) < Предыдущий =  Ниже максимального пика =  Clipped peaks =  Без изменений Файл с таким именем уже существует. Вы хотите его перезаписать? Подробное описание профиля Новый пресет был успешно импортирован Доступна новая версия - v.{0}
 Набор полезных инструментов для преобразования аудио и видео. Сохраните свои профили и повторно используйте их с помощью Менеджера Пресетов. Краткое имя профиля В файлах, поставленных в очередь, должен быть выбран один файл Полезный инструмент для поиска тем справки FFmpeg и опций A/V Конвертация	Shift+V AV-конвертация Прервать Информация об Videomass Активирует нормализацию на основе среднеквадратичного значения, которая на основе средней громкости вычисляет величину усиления для достижения той же средней мощности сигнала. Активирует нормализацию пикового уровня, в результате чего максимальный пиковый уровень будет равен заданному целевому уровню. Выполняет двухпроходную нормализацию. Нормализуйте воспринимаемую громкость с помощью фильтра "​loudnorm", который реализует алгоритм EBU R128. Включить row-mt 1 Добавить Добавить файлы Добавить Профиль Добавить новый профиль из этой панели с текущими настройками Добавьте метаданные
в файл Дополнительные аргументы Расширенные настройки Все пресеты по умолчанию были успешно восстановлены Все пресеты успешно экспортированы Уже существующие файлы: 

{}

Вы хотите перезаписать?  Элемент должен быть выбран в контрольном списке URL-адресов Вид Язык приложения Настройки приложения Применить Применить фильтры шумоподавления Вы уверены, что очистили папку "Корзина"? Вы действительно хотите удалить выбранный файл log-журнала? Вы уверены, что хотите удалить выбранный профиль? Восстановить его уже не удастся. Вы уверены, что хотите выйти из приложения? Вы уверены, что хотите удалить пресет "{}" ?

Он будет перемещен в подпапку «Удалённые» папки пресетов. Пропорции Добавить необязательный суффикс к именам выходных файлов: По крайней мере один " Код формата " должен быть проверен для каждого URL, выделенного зеленым цветом. Для объединения требуются как минимум два файла. Внизу Слева Справа Вверху (по умолчанию) Аудио Кодек аудио Аудио кодировщик Аудио фильтры Свойства аудио Аудио потоки Отображение аудиопотоков Нормализация звука требуется только для некоторых файлов Нормализация звука отключена Процессы нормализации звука не могут быть сохранены в Менеджере Пресетов. Нормализация звука не будет применяться, потому что она равна источнику Автоматически создавать подпапки при загрузке плейлистов Автоматическое определение Автоматический выход после воспроизведения Доступные видеокодеки. "Копировать" не является кодеком, но указывает на то, что видеопоток не должен перекодироваться, и позволяет изменить формат или другие параметры Назад Пакетное переименование	Ctrl+B Быть осторожен! Выбранный пресет будет заменен пресетом по умолчанию. Ваши профили могут быть удалены!

Вы хотите продолжить? Быть осторожен! Это действие вернет все пресеты к значениям по умолчанию. Ваши профили могут быть удалены!

Вы хотите продолжить? Битовая глубина - это количество бит информации в каждом образце, и она напрямую соответствует разрешению каждого образца. Битовая глубина имеет значение только в отношении цифрового сигнала PCM. Форматы без PCM, такие как форматы сжатия с потерями, не имеют связанной битовой глубины. Цвет поля Назначив дополнительный суффикс, вы можете избежать перезаписи файлов ВОЗМОЖНОСТЬ ДЕКОДИРОВАНИЯ Папка кеша Невозможно продолжить: продолжительность на шкале времени превышает продолжительность некоторых файлов в очереди. Невозможно сохранить текущие данные в файл '{}'. Центр Изменение размера и цвета отметки времени во время воспроизведения Изменения вступят в силу после перезапуска программы

Вы хотите выйти из приложения сейчас? Проверить наличие новых пресетов Проверить последнюю версию Проверьте последнюю версию Videomass на <https://pypi.org/project/videomass/> Проверяйте новые версии пресетов Videomass с домашней страницы Проверьте текущий вывод или прочтите соответствующий файл журнала для получения дополнительной информации. Проверить последнюю версию Выберите папку для загрузки Выберите папку для экспорта всех пресетов Выберите папку для сохранения выбранного пресета Выберите новое место для файлов, которые нужно удалить Выберите пресет и просмотрите его профили Выберите временное место назначения для конверсий Выберите временное место для загрузок Выберите тему из списка Выберите индекс из доступных аудиопотоков. Если исходным файлом является видео, рекомендуется выбрать цифровой аудиоиндекс. Если исходным файлом является аудиофайл, оставьте этот элемент управления в положении "Авто". Выберите исполняемый файл {} Очистить все включенные фильтры  Очистить выбранный log-журнал Очистить кеш при выходе из приложения Нажмите на столбец "Элементы плейлиста", чтобы указать индексы(номера) видео в плейлисте, разделенные запятыми, например: "1,2,5,8", если вы хотите загрузить видео под индексами 1, 2, 5, 8 в списке воспроизведения.

Вы можете указать диапазон: "1-3,7,10-13" - будет загружать видео с индексами 1, 2, 3, 7, 10, 11, 12 и 13.
 Нажмите кнопку "Далее" Нажмите кнопку "Следующий", чтобы начать Закрыть Videomass Столбцы: Concatenate Demuxer	Shift+D Объединить медиафайлы Объединение нескольких медиафайлов в соответствии с порядком импорта без перекодирования Папка конфигурации Подтвердить настройки Поздравляю! Вы уже используете последнюю версию.
 Считайте преобразования относительно предыдущего кадра, если отмечен, и абсолютными, если не отмечен. По умолчанию отмечен. Постоянный коэффициент скорости. Меньшие значения = более высокое качество и больший размер файла. Установите -1, чтобы отключить эту проверку. Сохраните пропорции (сохраните соотношение сторон) Контейнер: Папка конверсий	Ctrl+C Конвертировать Конвертировать с помощью FFmpeg Создать новый пресет Создать новый профиль Создайте новый профиль и сохраните его в выбранном пресете Создайте новый профиль на пресете "{}" Создать анимированный GIF Создание эскизов Создание мозаики из плиток Создавайте видео из последовательности изображений, основываясь на порядке импорта, с возможностью добавления аудиофайла. Cоздавайте, редактируйте или используйте свои любимые пресеты и профили прямо сейчас с полной поддержкой форматов и кодеков FFmpeg. Фильтр Обрезка Обрезать по высоте Обрезать по ширине Обрезка Выбор области обрезки  Многоплатформенный графический интерфейс для FFmpeg и youtube-dl.
 Текущий путь: ВОЗМОЖНОСТЬ ДЕКОДИРОВАНИЯ ОТКЛЮЧЕНО: Формат данных Время/Качество Декодеры Папки назначения по умолчанию успешно восстановлены Положение по умолчанию Деинтерлейсинг Деинтерлейсинг входного видео с помощью фильтра `w3fdif` Деинтерлейсируйте входное видео с помощью фильтра yadif. Используя FFmpeg, это лучший и самый быстрый выбор Деинтерлейсинг с фильтром w3fdif Деинтерлейсинг c фильтром 'yadif' Деинтерлейсинг Деинтерлейсинг(прогрессивная) и чересстрочная развёртка Удалить Удалить все файлы из списка Удалите все файлы в папке корзины Videomass Удалить весь текст из списка Удалите содержимое log-журналов при выходе из приложения Удалить выбранный профиль Только демультиплексирование Поддержка демультиплексирования/мультиплексирования Шумоподавитель Фильтры шумоподавления Знаменатель Описание Папка назначения Каталоги не разрешены, добавьте просто файлы. Отключено Соотношение сторон дисплея. Установите 0, чтобы отключить Отображение выбранных элементов в текстовом формате Отображать отметку времени Отображает временную метку при воспроизведении фильмов с FFplay Больше не показывать этот диалог Пожертвование Выполнено! Не проверять SSL-сертификат Скачать Загрузить только аудиофайл Загрузите все пресеты Videomass локально с домашней страницы Скачать все доступные субтитры Скачать все видео в плейлисте Скачать код формата Загрузите файлы с помощью youtube-dl Скачать аудио и видео раздельно Скачать только субтитры Загрузите последние пресеты Загрузка видео по разрешению Загрузчик Настройки загрузчика Папка загрузок	Ctrl+D Перетащите один или несколько файлов ниже Дублировать Дубликаты файлов отклоняются: > {} Длина Продолжительность: ВКЛЮЧЕНО: ОШИБКА ОШИБКА: Неверный URL-АДРЕС: "{}" ОШИБКА: недопустимая опция ОШИБКА: Отсутствует аудиопоток:
"{}" ОШИБКА: Найдено несколько одинаковых URL-адресов ОШИБКА: опечатка в ключе JSON: {}

Файл: "{}"
Неправильный ключ ? ОШИБКА: Не удается получить коды формата.

Неподдерживаемый URL-адрес:
'{0}' ERROR: {0}

{1} не установлен, используйте диспетчер пакетов, чтобы установить его. Легко загружайте видео и аудио в различном формате и качестве с YouTube, Facebook и других сайтов. Редактировать Редактировать профиль в пресете "{}" Редактировать выбранный профиль Добавить миниатюру
в аудиофайл Корзина пуста Включить одно неподвижное изображение Включить другое место для запуска FFmpeg Включить другое место для запуска FFplay Включить другое место для запуска FFprobe Включить фильтр hqdn3d Включить фильтр nlmeans Включить stabilizer Включить текстовый формат Включите режим виртуального штатива, если этот флажок установлен, что эквивалентно relative=0:smoothing=0. По умолчанию флажок не установлен. Используйте также вариант штатива в vidstabdetect. Включено Кодеры Время окончания отрезка в 24-часовом формате (HH:MM:SS) Конец: Введите URL-адреса ниже Введите имя для нового пресета Вводите только буквенно-цифровые символы. Вы также можете использовать дефис ("-") и подчеркивание ("_"). Пробелы не допускаются. Ошибка, недействительный пресет: "{}" Выход Выход	Ctrl+Q Экспортировать все... Экспорт всей папки пресетов как копии Экспорт выбранного Экспорт выбранного пресета как копии Расширение Извлекайте изображения (кадры) из ваших фильмов в форматы JPG, PNG, BMP, GIF. FFmpeg Конфигурация FFmpeg Декодеры FFmpeg Документация FFmpeg Кодировщики FFmpeg Форматы файлов FFmpeg Разделы справки FFmpeg Комплект log-журналов FFmpeg FFplay FFplay   ...не найдено ! FFprobe   ...не найдено ! ВЫБОР ФАЙЛА Фатальная ошибка ! Файл Имя Файла Файл Файл не существует или недействителен:  %s Файл не существует:

"{}"
 Файл содержит недопустимые символы, такие как: Список файлов изменен, проверьте настройки еще раз. Имя файла Переименование файла... Файл для объединения
Имя выходного файла
Папка назначения
Выходной формат
Временной период Файл для обработки
Выходное имя файла
Папка назначения
Формат вывода
Дополнительные аргументы
Аудиофайл
Shortest(аудио-отрывок)
Изменение размера
Предварительный ввод
Кадров в секунду (FPS)
Длительность неподвижного изображения
Общая продолжительность видео Файл без расширения формата: укажите соответствующее расширение имени файла, например «.mkv», «.avi», «.mp3» и т.д. Завершено Первый проход выбранного профиля Вы должны выбрать профиль в списке Сначала выберите тему в раскрывающемся списке Флаги отключены Флаги включены Настройки флагов Цвет шрифта Размер шрифта Подробнее см. Руководство пользователя Videomass: Для получения дополнительной информации посетите официальную документацию FFmpeg: Принудительно использовать исходное соотношение 
 сторон, используя отступы, а не растяжение Код формата Форматы должны быть разделены запятыми Кадры повторяются заданное количество раз в секунду. В некоторых странах это 30 для NTSC, другие страны (например, Италия) используют 25 для PAL Картинки из фильма Картинки из фильма	Shift+S Из последовательности изображений в видеофайл Собирает информацию о мультимедийных потоках Создает парное видео для сравнения Получить последнюю версию Получить версию вашей операционной системы, версию Python и wxPython. Получите ваши файлы по указанному вами адресу Получает данные о максимальной и средней громкости в dBFS, затем вычисляет величину смещения для нормализации звука. Перейдите на панель 'A/V Конвертация' Перейдите на панель 'Concatenate Demuxer' Перейдите на панель 'Картинки из фильма' Перейдите к 'Начальной панели' Перейдите на панель 'Менеджер пресетов' Перейдите на панель 'Still Image Maker' Перейдите на панель 'YouTube Downloader' Перейти к следующей панели Перейти к предыдущей панели Перейти Высота Высота: Помощь Программа просмотра справки Начальная панель	Shift+H Как установить pip в ваш дистрибутив Linux Как обновить пакет Python Размер значка: Темы значков Если FFmpeg отсутствует на вашем компьютере, это приложение
будет невозможно использовать Если включен, скрывает некоторые выходные сообщения. Если этот флажок установлен, окно FFplay автоматически закроется в конце воспроизведения Если вы уже установили FFmpeg в своей операционной
системе, щелкните кнопку "Автоопределение". Если вы хотите сохранить соотношение сторон, выберите «Сохранить пропорции» ниже и укажите только одно измерение,
ширину или высоту, и установите для другого измерения значение -1 или -2 (для некоторых 
кодеков требуется -2, так что сначала вы должны провести некоторое тестирование) Если вы хотите использовать версию FFmpeg, которая
находится в вашей файловой системе, но не установлена
​​в вашей операционной системе, нажмите кнопку "Найти". Игнорировать регистр Не обращайте внимания на различие в регистре: символы с другим регистром будут совпадать. Изображения должны быть изменены, пожалуйста, используйте функцию изменения размера. Импортировать группу пресетов из папки и обновить существующие Импортировать новый пресет Импортировать новый пресет или обновить существующий Импортировать новую папку пресетов Импорт группы Импортировать пресет Включить аудиофайл Включите идентификатор видео
в имена файлов Неполные данные профиля Выбор индекса: Информация Интегрированная цель громкости в LUFS. От -70,0 до -5,0, по умолчанию -24,0 Чересстрочная Чересстрочная с фильтром 'interlace' Процесс прерван ! Недопустимый файл: '{}' Недопустимое имя пути. Содержит двойные или одинарные кавычки Загружен недопустимый пресет.
Рекомендуется удалить его или переписать в формат JSON, совместимый с Videomass.

Возможное решение: откройте панель Менеджер Пресетов, перейдите в столбец пресеты и попробуйте нажать кнопку «Восстановить» Инвертировать преобразования, если отмечено. По умолчанию флажок не установлен. 'ffmpeg' установлен в вашей системе? Сообщить о проблеме Это может уменьшить размер файла, но занимает больше времени. Отслеживает вывод для ошибок отладки Уровень Ограничитель максимального пикового уровня или среднего уровня (при переключении на RMS) в dBFS. От -99,0 до +0,0; по умолчанию для уровня PEAK -1,0; по умолчанию для RMS -20.0 Прослушивание Загрузить Загрузить фрейм Найдите Поиск исполняемых файлов FFmpeg
 Список log-файлов Папка журнала Log-журнал Целевой диапазон громкости в LUFS. От +1,0 до +20,0, по умолчанию +7,0 Убедитесь, что вы используете последнюю доступную версию '{}'.
Это позволяет избежать проблем с загрузкой.
 Map: Максимальная громкость dBFS Максимальный истинный пик в dBTP. От -9,0 до +0,0, по умолчанию -2,0 Средняя громкость dBFS Мультимедийные 
         потоки Тип медиа Медиа: Разное Горизонтальное положение верхнего левого угла Переместите исходный файл в папку корзины Videomass после успешного кодирования Вертикальное положение верхнего левого угла Анализатор мультимедийных потоков Мультиплексоры и Демультиплексоры Доступны мультиплексоры и демультиплексоры для используемого FFmpeg. Только мультиплексирование Имя Имя уже используется: Имя содержит недопустимые символы, такие как: {0} Новый Новое имя # Новый размер в пикселях Вперед Следующий > Нет звука Нет файлов для удаления Нет новой версии Пресеты не найдены.

Возможное решение: откройте панель Менеджер Пресетов, перейдите в столбец пресеты и попробуйте нажать кнопку «Восстановить все ...» Нет такой папки '{}' Нормализация Не установлено Завершено, но не все прошло хорошо. Не найдено Числитель ОК: индексы для загрузки Выключено Смещение dBFS Одно или несколько имен форматов, разделенных запятыми, для включения в профиль Один проход Первый проход, Не начинайте с: `ffmpeg -i filename`; не заканчивайте `output-filename` Одновременно можно воспроизводить только одно видео.

Неподдерживаемый '{0}':
'{1}' Открыть аудиофайл Открыть аудиофайл Откройте один или несколько файлов Открыть временную папку конверсий Открыть временную папку загрузок Открыть папку корзины Videomass, если она существует Открыть папку загрузок по умолчанию Открыть папку по умолчанию для конверсий файлов Открыть выбранные файлы Открыть папку временных загрузок Открыть временную папку конверсионных файлов Открыть...	Ctrl+O Открывает папку кэша Videomass, если она существует Открывает папку конфигурации Videomass Открывает папку журнала Videomass, если она существует Оптимизация Параметры Другие опции Другие неофициальные ресурсы: Выходной формат Выходной формат. Пусто для копирования формата и кодека. Не указывать точку `.` Выходной экран	Shift+O Имя выходного файла Выходная папка не существует:

"{}"
 Расширение формата вывода. Оставьте пустым, чтобы скопировать кодек и формат Формат вывода. Он также представляет собой расширение выходного файла. Выходной формат: Статистика звука на основе PEAK Параметры Путь к исполняемым файлам В доступе отказано: {}

Проверить разрешения на выполнение. Разместите панель инструментов Воспроизведение Воспроизведение и прослушивание результатов аудиофильтров Воспроизвести выбранный URL Воспроизведение выбранного в списке файла Воспроизведение видео до окончания звуковой дорожки(трека) Редактор плейлистов(playlist) Элементы плейлиста Видео элементы плейлиста для загрузки Пожалуйста подтвердите Пожалуйста, подтвердите Пожалуйста, найдите время, чтобы настроить приложение Ссылки после нормализации: Предварительно скомпилированные видео Основные настройки	Ctrl+P Предпочтительный формат видео Пресет Пресеты Менеджер Пресетов Менеджер Пресетов	Shift+P Предотвратить перезапись
файлов Предпросмотр Предварительный просмотр с учётом фильтров FFplay Журнал процесса: Обработка ... Профиль Имя профиля Профиль с таким именем уже сохранен Профили Качество Качество/Скорость
править соотношение: Queued File
Выходной формат
Веб-оптимизация
Кодек Аудио
Битрейт аудио
Аудио каналы
Скорость звука
Бит на образец
Нормализация звука
Временной отрезок
Выбор входного аудио индекса Queued File
Проход кодировки
Используемый профиль
Выходной формат
Временной отрезок Queued File
Веб-оптимизация
Выходной формат
Кодек видео
Соотношение сторон
FPS
Кодек Аудио
Аудио каналы
Скорость звука
Битрейт аудио
Бит на образец
Нормализация звука
Выбор входного аудио индекса
Output Audio Map
Индекс потока субтитров
Временной отрезок Queued File
Веб-оптимизация
Проход кодирования
Формат вывода
Видео кодек
Битрейт видео
CRF
Минимальный рейт
Максимальный рейт
Размер буфера
VP8/VP9 Параметры
Видео фильтры
Соотношение сторон
FPS
Пресет
Профиль
Tune
Аудиокодек
Аудио каналы
Аудио рейт
Битрейт аудио
Бит на образец
Нормализация звука
Индекс выбранного входного аудио
Output Audio Map
Индекс потоков субтитров
Временной отрезок Статистика звука на основе RMS Требуется перезапуск Читайте и пишите полезные заметки и напоминания. Готово Наименование Обновить все файлы log-журналов Перезагрузить Помните, что вы всегда можете изменить эти настройки позже в диалоговом окне 'Настройка'. Удалить Удалить выбранные файлы из списка Удалить выбранный пресет из Менеджера Пресетов Переименуйте все файлы, перечисленные на панели «Файлы в очереди» Переименовать элементы Переименуйте файл, выбранный на панели «Файлы в очереди» Переименовать выбранный файл	Ctrl+I Переименуйте выбранный файл в: Файлов для переименования: {0} Заменить выбранный пресет на стандартный для Videomass Необходимо: Microsoft Visual C++ 2010 Service Pack 1 Redistributable Package (x86) Зарезервированные аргументы для первого прохода Зарезервированные аргументы для второго прохода Сброс Изменение размера Фильтры изменения размеров Разрешение Восстановить Восстановить все... Восстановить папки назначения по умолчанию Восстановить папки по умолчанию для конверсий и загрузки файлов Ограничить имена файлов Результат DBFS Возвратить все пресеты Videomass в состояние по умолчанию Повернуть на 180 градусов Повернуть на 90 градусов влево Повернуть на 90 градусов вправо Вращение Настройка вращения Ряды: Образец (в пикселях) соотношения сторон.
Установите 0, чтобы отключить Сохраните все загрузки во временной папке Сохраняет каждый файл в той же папке, что и входной файл Сохраните новый профиль в ... Сохранить.. Масштабный фильтр, установите 0, чтобы отключить Поиск определенного кадра Второй проход выбранного профиля Выберите файл журнала Выберите элементы, чтобы просмотреть их в текстовом формате Выбранный файл
Выходной формат
Папка назначения
Скорость (кадров в секунду)
Изменение размера
Мозаичных рядов
Мозаичных колонок
Мозаика (padding)
Мозаика (margin)
Custom Arguments
Временной период Выбранный индекс не существует или не содержит никаких аудиопотоков Установите контроль FPS от 0,1 до 30,0 кадров в секунду. Чем выше это значение, тем больше изображений будет извлечено. Установите постоянное место для сохранения экспортированных файлов Установите постоянное место для сохранения скачанных файлов Установите, насколько тряским будет видео и насколько быстрая камера. Значение 1 означает небольшую неустойчивость, значение 10 означает сильную неустойчивость. Значение по умолчанию - 5. Установите целевое значение интегрированной громкости: Set loudness range target: Установите максимальный угол в радианах (градусы * PI / 180) для поворота кадров. Значение по умолчанию -1, что означает отсутствие ограничений. Set maximum true peak: Установите минимальный контраст. Ниже этого значения локальное поле измерения отбрасывается. Диапазон 0-1. Значение по умолчанию - 0,25. Установите оптимальное масштабирование, чтобы избежать границ. Допустимые значения: «0» отключено, «1» определяется оптимальное значение статического масштабирования (только очень сильные движения приведут к видимым границам) (по умолчанию), «2» определяется оптимальное значение адаптивного масштабирования (границы не видны), см. zoomspeed. Обратите внимание, что значение, указанное при увеличении, добавляется к рассчитанному здесь. Установите процент для максимального увеличения каждого кадра (активируется, когда для optzoom установлено значение 2). Диапазон от 0 до 5, значение по умолчанию - 0,25. Установите процент для увеличения. Положительное значение приведет к эффекту увеличения, отрицательное значение - к эффекту уменьшения. Значение по умолчанию - 0 (без увеличения). Установите номер опорного кадра для штатива режима. Если этот параметр включен, движение кадров сравнивается с опорным кадром в отфильтрованном потоке, идентифицируемым указанным номером. Идея состоит в том, чтобы компенсировать все движения в более или менее статичной сцене и сохранять неподвижность обзора камеры. Фреймы считаются начиная с 1. Установите размер шага процесса поиска. Область около минимума сканируется с разрешением 1 пиксель. Значение по умолчанию - 6. Установите точность процесса обнаружения. Значение 1 означает низкую точность, значение 15 означает высокую точность. Значение по умолчанию - 15. Установите алгоритм оптимизации пути камеры. Значения: «gauss»: фильтр нижних частот ядра Гаусса при движении камеры (по умолчанию) и «avg»: усреднение при преобразованиях Устанавливает коэффициенты чересстрочного фильтра. Установите количество кадров (значение * 2 + 1), используемых для фильтрации нижних частот движений камеры. Значение по умолчанию - 15. Например, число 10 означает, что используется 21 кадр (10 в прошлом и 10 в будущем) для сглаживания движения в видео. Большее значение приводит к более плавному видео, но ограничивает ускорение камеры (движения панорамирования / наклона). 0 - это особый случай, когда моделируется статическая камера. Установите интервал времени между изображениями в секундах (от 1 до 100 секунд), по умолчанию - 1 секунда Настройте временную папку для конверсий Настройте временную папку для загрузок Setdar (соотношение сторон экрана)  SetSar(примерное соотношение сторон) Настройки шкалы отметки времени Настройки Цвет тени Увеличьте резкость или размытие входного видео. Обратите внимание на использование нерезкого фильтра, который всегда рекомендуется. Сочетания клавиш во время проигрывания с FFplay Показать встроенные возможности настройки FFmpeg Показать log-журналы	Ctrl+L Показать конфигурацию Показать текстовое поле Показать последнюю версию... Показывать полезные сочетания клавиш при воспроизведении или предварительном просмотре с FFplay Просмотр log-журналов Показывает доступные декодеры для FFmpeg Показывает доступные кодировщики для FFmpeg Показывает статистику и информацию о загрузке Показывает последнюю версию, доступную на github.com Показывает текст на кнопках панели инструментов Показывает используемую версию Простой фильтр с чересстрочной разверткой из прогрессивного содержимого. Размер Некоторые текстовые поля все еще не заполнены Извините, задача не выполнена ! Извините, этот пресет не входит в стандартный набор настроек Videomass. Размер источника: {0} x {1} пикселей Пространства вокруг границ мозаики. От 0 до 32 пикселей Пространства вокруг мозаичных плиток. От 0 до 32 пикселей Задает максимальный допуск. Используется только в сочетании с размером буфера Укажите минимальный допуск для использования Определяет размер буфера декодера, который обусловливает изменчивость выходного битрейта  Укажите, как работать с границами, которые могут быть видны из-за компенсации движения. Возможные значения: «keep» сохранить информацию об изображении из предыдущего кадра (по умолчанию), «black» заполнить границу чёрным Укажите тип интерполяции Укажите тип интерполяции. Доступные значения: «no» без интерполяции, «linear», линейный, только горизонтальный, «bilinear», линейный в обоих направлениях (по умолчанию), «bicubic», кубический в обоих направлениях (медленно). Укажите кадры для деинтерлейсинга. Stabilizer Время начала отрезка в 24-часовом формате (HH:MM:SS) Начало: Статистика Просмотр статистики Still Image Maker Still Image Maker	Shift+I Останавливает текущий процесс Продолжительность трансляции всегда относится к файлу с наибольшей продолжительностью.
 В противном случае для него будет установлено значение {0} Продолжительность трансляции. Нажмите на меня, чтобы узнать подробности. Субтитры Потоки субтитров Успешные изменения! Успешное восстановление Профиль успешно сохранен! Успешно завершено ! Успешно загружено в "{0}" Список поддерживаемых форматов Список поддерживаемых форматов (необязательно). Не указывать точку `.` Подавить избыточный вывод Система Версия системы ВЫБОР НАЗВАНИЯ Target Целевой уровень: Благодарю вас! URL-адреса содержат видеоканалы. Вы уверены, что хотите продолжить? URL-адреса содержат списки воспроизведения. Вы уверены, что хотите продолжить? Частота дискретизации звука (или частота дискретизации) - это частота дискретизации звука, которая измеряется в герцах. Чем выше частота, тем более верным будет источник звука и тем больше будет увеличиваться размер файла. Для воспроизведения аудио CD установите частоту дискретизации 44100 кГц. Если вы не уверены, установите значение «Авто», и исходные значения будут скопированы. Битрейт звука влияет на сжатие файла и, следовательно, на качество прослушивания. Чем выше значение, тем выше качество. Выбранная тема значка изменит только значки, фон
и передний план некоторых текстовых полей. Загрузчик отключен. Проверьте свои настройки. Файл не является кадром или видеофайлом Файлы не имеют одинаковых «codec_types», «sample_rate» или одинаковых «ширины» или «высоты». Невозможно продолжить. Следующие настройки влияют на выходные сообщения и
сообщения журнала во время процессов перекодирования.
Изменяйте, только если знаете, что делаете.
 Пресет "{0}" был успешно удален Пресет был успешно экспортирован База данных пресетов успешно обновлена Процесс был остановлен из-за фатальной ошибки. Функция поиска позволяет находить записи в текущей теме Управление выбранным профилем изменено вручную.
Вы хотите применить его в процессе конвертации? Выбранный профиль не подходит для следующих форматов файлов:

%s

 Размер отметки времени не подстраивается автоматически под размер видео, вы должны установить размер здесь Есть еще запущенные процессы ... Если вы хотите остановить их, используйте кнопку "Прервать".

Вы хотите закрыть приложение? Доступна новая версия {0} Эта функция позволяет загружать видео и аудио из
многих сайтов, включая YouTube.com и даже Facebook. Этот фильтр несовместим с включенным двухпроходным режимом Этот пресет уже существует и скоро будет обновлен. Не волнуйтесь, все ваши профили будут сохранены.

Вы хотите продолжить? Это обновит базу пресетов. Не волнуйтесь, все ваши сохраненные профили не пропадут.

Вы хотите продолжить? Потоки, используемые для перекодирования (от 0 до 32): Настройки отметки времени Заглавие Для выхода нажмите кнопку "Завершено" Настройка панели инструментов Инструменты Список тем... Перевод... Фильтр вращения Корзина Trim Звук Два прохода Второй проход (по желанию), Не начинайте с `ffmpeg -i filename`; не заканчивайте `output-filename` URL Список URL-адресов изменен, проверьте настройки еще раз. URL-адреса не имеют ссылок на плейлисты Не удается получить коды формата для '{0}'

Неподдерживаемый '{0}':
'{1}' Невозможно просмотреть Stabilizer видеофильтр Не обнаружены значения громкости! Нажмите кнопку «Обнаружение громкости», чтобы проанализировать данные о громкости звука. Непредвиденная ошибка при создании файла:

{0} Непредвиденная ошибка при удалении содержимого файла:

{0} Не установлено Фильтр Нерезкость: Неподдерживаемый формат '{}' Обновить список пресетов Url Используйте временное местоположение для сохранения конверсий Использовать для Web Онлайн помощь Используемая версия Версия {} Видео Кодек видео Обнаружение видео Кодировщик видео Фильтры Видео Видео поток Фильтр стабилизатора видео(stabilizer) Преобразование видео Соотношение ширины и высоты видео. Videomass Videomass - AV-конвертация Videomass - Concatenate Demuxer Videomass - Скачивание... Videomass - Картинки из фильма Videomass - Загрузка... Videomass - Выходной монитор Videomass - Менеджер Пресетов Videomass - Файлы в очереди Videomass - URL в очереди Videomass - Still Image Maker Videomass - YouTube Downloader Videomass Помощник Videomass кажется уже включает FFmpeg.

Вы хотите использовать это? Videomass имеет простой графический
интерфейс для youtube-dl и yt-dlp
 Videomass - это приложение на основе FFmpeg
 Videomass поддерживает моно и стерео аудиоканалы. Если вы не уверены, установите «Авто», исходные значения будут скопированы. Videomass: Пожалуйста подтвердите Посмотр Просмотр последнего log-журнала Просмотр сообщений log-журнала Статистика громкости Обнаружение громкости ВНИМАНИЕ! Выбранный URL-адрес не относится к списку воспроизведения. Индексировать можно только строки, отмеченные зелёным. Пожалуйста, подождите....
Анализ звуковых пиков. Пожалуйста, подождите....
Получить запрошенные данные. Подождите...
Архив скачивается Предупредить при выходе Предупреждение Добро пожаловать в Videomass Добро пожаловать в Помощник Videomass! ...Это означает, что результирующий звук не
изменится, потому что он равен источнику. ...Это означает, что результирующий звук будет 
обрезан, потому что его громкость превышает
максимальный уровень 0 дБ. Это приводит к 
потере данных и искажению звука. ...Это означает, что аудиосигнал будет воспроизводиться
с меньшей громкостью, чем исходный. Когда недоступно, выбранное разрешение видео будет заменено ближайшим Где вы предпочитаете сохранять загрузки? Где вы предпочитаете сохранять перекодировки? Какой размер отрегулировать? Клавиши воспроизведения Ширина Ширина: Wiki Помощник успешно завершен!
 Рабочие заметки	Ctrl+N Напишите субтитры
к видео Вы используете '{0}' версии {1} Вы используете версию для разработки, которая еще не выпущена!
 YouTube Downloader YouTube downloader	Shift+Y [ОБРЕЗАТЬ] Укажите, как работать с границами при компенсации движения [ИНВЕРТИРОВАТЬ] Инвертировать преобразования, если отмечено [OPTALGO] алгоритм оптимизации [RELATIVE] Считать преобразования относительно предыдущего кадра, если отмечен; абсолютные, если не отмечен [ШТАТИВ2] виртуальный режим штатива, эквивалентный relative=unchecked:smoothing=0 [ШТАТИВ] Включите режим штатива, если установлен флажок [Videomass]: НЕ УДАЛОСЬ ! [Videomass]: УСПЕШНО ! папка кеша еще не создана. описание флаги формат fps список тем справки параметры фильтра hqdn3d hqdn3d:
 Это высокоточный и качественный трехмерный шумоподавляющий фильтр. Стремится уменьшить шум изображения, создать плавные изображения и сделать статическиеизображения неподвижными. Понятно, что это должно улучшить визуальное качество. список приемников устройства вывода перечисляет источники устройства ввода lowpass:
Включите (по умолчанию) или отключите вертикальный фильтр нижних частот, чтобы избежать чересстрочной развертки твиттера и уменьшить муаровые узоры.
По умолчанию настройка отсутствует. режим
Используемый режим чересстрочной развертки. параметры фильтра nlmeans nlmeans:
Удаление шума из кадров с использованием алгоритма нелокальных средств позволяет восстанавливать видеопоследовательности даже с сильным шумом. Он идеально подходит для улучшения качества старых кассет VHS. параметры четность
Предполагаемая четность поля изображения для входного чересстрочного видео. пожалуйста, попробуйте снова. распечатать все варианты (очень много) основные параметры печати распечатать больше вариантов q, ESC
f
p, SPC
m
9, 0
/, *
a
v
t
c
w
s

влево/вправо
вниз/вверх
страница вниз/страница вверх

щелчок правой кнопкой мыши
двойной щелчок левой кнопкой мыши развертка:
определяет, взят ли чересстрочный кадр из четных (tff - по умолчанию) или нечетных (bff) строк прогрессивного кадра. показать доступные методы ускорения HW показывать доступные форматы аудиофайлов показывать доступные фильтры битового потока показывать доступные названия цвета показывать доступные устройства показывать доступные фильтры показать доступные форматы пикселей показывать доступные протоколы определяет целевую (в среднем) скорость передачи данных для кодировщика. Высокая значение = высокое качество. Установите значение -1, чтобы отключить эту проверку. начало  {} | продолжительность  {} статус подождите ... я прерываю подождите... все операции будут остановлены в конце текущей загрузки. {0}: Доступна последняя версия: {1} {}

Извините, удаление не получилось, продолжение невозможно. {} файл в очереди 